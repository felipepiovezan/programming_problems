#include <initializer_list>

template<typename T> class printer;

int main(int argc, char **argv) {
    int x = 6;
    printer<decltype(x)> x_type;

    auto &func_ref = main;
    printer<decltype(func_ref)> func_ref_type;

    auto func_ptr = main;
    printer<decltype(func_ptr)> func_ptr_type;

    auto *func_ptr_with_ptr = main;
    printer<decltype(func_ptr_with_ptr)> func_ptr_with_ptr_type;

    const auto char_ptr = "hola";
    printer<decltype(char_ptr)> char_ptr_type;

    auto init_list = {1};
    printer<decltype(init_list)> init_list_type;
    printer<decltype((init_list))> init_list_type2;
    
    auto an_int{1};
    printer<decltype(an_int)> an_int_type;

    auto lambda = [](int n) {return 1.0;};
    printer<decltype(lambda)> lambda_type;
}
