#include <functional>
#include <memory>

class Widget{};

std::function<bool(const std::unique_ptr<Widget>&,
        const std::unique_ptr<Widget>&)> func;

//Because lambda expressions yield callable objects, closures can be stored in
//std::function objects.

std::function<bool(const std::unique_ptr<Widget>&,
        const std::unique_ptr<Widget>&)> derefUPLess =
            [](const std::unique_ptr<Widget>& p1,
               const std::unique_ptr<Widget>& p2)
            { return *p1 < *p2; };

// This is extremely verbose!
// And it is NOT the same as:

auto lambda = [](const std::unique_ptr<Widget>& p1,
                 const std::unique_ptr<Widget>& p2)
                 { return *p1 < *p2; };

// Using auto will only take as much space as required by the closure.
// An std::function has fixed space for any given signature, which might
// not me enough for the closure, requiring heap allocation.

// And, thanks to implementation details that restrict inlining and yield
// indirect func‐ tion calls, invoking a closure via a std::function object is
// almost certain to be slower than calling it via an auto-declared object


// auto should also be used to avoid issues with 32 vs 64 bit systems:

std::vector<Widget> v;
unsigned size = v.size();
auto actual_size = v.size();

// The first declaration of size is wrong if we are in a 64 bit system:
// std::vector<Widget>::size_type is guaranteed to be wide enough to hold
// vectors of any size allocable in the system.


// Another issue: unintentional copies with auto.

std::unordered_map<std::string, int> map;

for (const std::pair<std::string, int> &item : map)
{}

// The type of std::pair in the hash table is std::pair<const std::string, int>,
// and forgetting to declare item like so will trigger the creation of a copy
// of each pair.
