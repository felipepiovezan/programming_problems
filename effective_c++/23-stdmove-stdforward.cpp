
// Given:
void f(Widget&& w);

// The parameter w is an lvalue even though its type is an rvalue-reference to
// Widget.

// A useful heuristic to determine whether an expression is an lvalue is to ask
// if you can take its address. If you can, it typically is. If you can’t, it’s
// usually an rvalue.

// Basic knowledge:
// std::move doesn’t move anything. std::forward doesn’t forward anything. At
// runtime, neither does anything at all. They generate no executable code.
// Not a single byte.


// std::move performs an unconditional cast to an rvalue.
// std::forward performs this cast conditionally.

// A possible implementation:
template<typename T>
decltype(auto) move(T&& param)
{
  using ReturnType = remove_reference_t<T>&&;
  return static_cast<ReturnType>(param);
};

// rvalues are USUALLY candidates for moving, but not always:
class Annotation {
  public:
    explicit Annotation(const std::string text)
      : value(std::move(text))
    {}
  private:
    std::string value;
};

// the move operation casts text to an rvalue, BUT it preserves the constness!
// So the compiler cannot call the string move constructor, because it only
// works for non-const string rvalues. The copy constructor is called instead.

// std::forward works similarly, but only casts IF the argument type was an
// rvalue reference:

void process(const Widget& lvalArg);
void process(Widget&& rvalArg); // process lvalues

template<typename T>
void logAndProcess(T&& param)
{
  auto now = std::chrono::system_clock::now(); // template that passes
  makeLogEntry("Calling 'process'", now);
  process(std::forward<T>(param));
}

// We don't want to cast param if it was passed as an lvalue reference!
// Notice that the rule #2 for template type deduction is that makes this
// possible: whether param was passed as a lvalue or rvalue is encoded there!

// You might ask whether we can dispense with std::move and just use
// std::forward everywhere. From a purely technical perspective, the answer is
// yes: std::forward can do it all. std::move isn’t necessary, but it is more
// explicit!


