#include<iostream>


// Although functions can’t declare parameters that are truly arrays, they can
// declare parameters that are references to arrays!
template <typename T, std::size_t N>
constexpr std::size_t get_array_size(T (&array)[N]) {
    return N;
}

template <typename T>
constexpr std::size_t alternative_get_array_size(T &array) {
    return sizeof(array)/sizeof(decltype(array[0]));
}

int main() {
    int an_array[100];
    std::cout << get_array_size(an_array) << std::endl;
    std::cout << alternative_get_array_size(an_array) << std::endl;

    // The first function is better because it makes the compiler error
    // much more explicit, compare:
    int a_non_array= 0;
    
    //std::cout << get_array_size(a_non_array) << std::endl;
    // error: no matching function for call to ‘get_array_size(int&)’
    // note:   template argument deduction/substitution failed:
    // note:   mismatched types ‘T [N]’ and ‘int’
    
    //std::cout << alternative_get_array_size(a_non_array) << std::endl;
    //In instantiation of ‘constexpr std::size_t alternative_get_array_size(T&)
    //[with T = int; std::size_t = long unsigned int]’:
    // error: subscripted value is neither array nor pointer
    //     return sizeof(array)/sizeof(array[0]);
}
