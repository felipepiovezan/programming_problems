std::vector<bool> features(const Widget& w);
void process_feature(bool b, const Widget &w);

Widget w;

bool has_feature1 = features(w)[0];
auto has_feature2 = features(w)[0];

process_feature(has_feature1, w); //ok.
process_feature(has_feature2, w); //undefined behavior.

// has_feature2 is of type std::vector<bool>::reference.
// features(w) is destroyed after has_feature2 is created, so when we invoke
// process_feature and the conversion to bool is performed, we have a 
// dangling reference.
// Fix: use the explicitly typed initializer idiom:
auto has_feature2 = static_cast<bool>(features(w)[0]);

// Also useful to indicate that the cast to a different type was intentional:

double calculateEpsilon();

float f1 = calculateEpsilon();  //sneaky!
auto f2 = static_cast<float>(calculateEpsilon()); // explicit!
