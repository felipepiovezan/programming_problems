// Rvalue references bind  only to objects that are candidates for moving.
// So you *know* that the object it's bound to may be moved. This is exactly
// what std::forward was created for.

class Widget {
  public:
    Widget(Widget&& rhs) : name(std::move(rhs.name)), p(std::move(rhs.p))
    { }

  private:
    std::string name;
    std::shared_ptr<SomeDataStructure> p;
};

// Universal references, on the other hand, might be eligible for moving: only
// when they were initialized with rvalues. Use std::forward in this case:

class Widget {
  public:
    template<typename T>
    void setName(T&& newName)
    { 
      name = std::forward<T>(newName); 
    }
};


//Never do this:

template<typename T>
void setName(T&& newName)
{
  name = std::move(newName);
}

std::string a_name = ...;
Widget w;
w.setName(n);  // n's value is now unknown.

// One could argue that setName shoud've been written with an overload:

void setName(const std::string& newName)
{ name = newName; }

void setName(std::string&& newName)
{ name = std::move(newName); }

// But there are several disadvantages to this:
// 1- It's a lot more code to maintain.
// 2- If there are many arguments, then the number of variations grown 
//    exponentially fast.
// 3- It could be less efficient:
     w.setName("Adela Novak");
//   would create a temporary std::string that would then me moved into 
//   w's data member. With the original version, the const char* value would
//   be passed directly into the std::string assignment operator. If we are 
//   dealing with types that are not cheap to move, then this becomes an issue.
// 4- Some function temples take an unlimited number of arguments!
    template<class T, class... Args>
    shared_ptr<T> make_shared(Args&&... args); 
    template<class T, class... Args>
    unique_ptr<T> make_unique(Args&&... args);
// Universal references is the only way to go here.


// Notice that you might want to avoid forwarding in cases where multiple
// uses of the object are necessary:
template<typename T>
void setSignText(T&& text)
{
  sign.setText(text);
  auto now = std::chrono::system_clock::now();
  signHistory.add(now, std::forward<T>(text));
}

// Only forward on the last use!

// If you’re in a function that returns by value, and you’re returning an object
// bound to an rvalue reference or a universal reference, you’ll want to apply
// std::move or std::forward when you return the reference.

Matrix operator+(Matrix&& lhs, const Matrix& rhs)
{
  lhs += rhs;
  return std::move(lhs);  //Move lhs into the return storage.
  // If Matrix doesn't support moving, a copy is performed as normal.
  // If the Matrix type ever supports moving, then simply recompiling the code
  // will give you performance.
}

// Similarly with universal references:
template<typename T>
Fraction
reduceAndCopy(T&& frac)
{
  frac.reduce();
  return std::forward<T>(frac);
}


// NEVER do this for local variables:
Widget makeWidget()
{
  Widget w;
  ...
  return std::move(w);
}

// Doing it, you make it illegal for the compiler to perform copy elision,
// an optimization where you construct the object being returned in the memory
// alloted for the function's return value. "return value optimization" (RVO)

// Compilers may elide the copying (or moving) of a local object in a function
// that returns by value if (1) the type of the local object is the same as
// that returned by the function and (2) the local object is what’s being
// returned.

// In the bad version above, look at the return statement. It returns 
// A REFERENCE to Widget, which fails condition (2). So compilers must move
// w. An attempt to optimize was turned into a limitation for the compiler.

// Sometimes performing RVO is hard and you may be wondering if the compiler
// will actually perform it. Even then, you shouldn't write the move. The
// standard says that, if the conditions for RVO are met but the compilers
// choose not to perform the copy ellision, THEN the object being returned
// must be treated as an rvalue. So the compiler HAS to move it anyway.

// The situation is similar for by-value function parameters. They’re not
// eligible for copy elision with respect to their function’s return value, but
// compilers must treat them as rvalues if they’re returned. As a result, if
// your source code looks like this,

Widget makeWidget(Widget w)
{
  ...
  return w;
}
// compilers must treat it as if it had been written this way:
Widget makeWidget(Widget w)
{
  ...
  return std::move(w);
}

 
