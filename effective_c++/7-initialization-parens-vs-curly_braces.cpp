Widget w1; // call default constructor
Widget w2 = w1; // not an assignment; calls copy ctor
w1 = w2; // an assignment; calls copy operator=

//Using braces, specifying the initial contents of a container is easy:
std::vector<int> v{ 1, 3, 5 };

// In some places, using () init is illegal:
class Widget {
    private:
        int x{ 0 }; // ok
        int y = 0;  // ok
        int z(0);   // error!
};

//In other places, = init is illegal! E.g. uncopyable objects:
std::atomic<int> ai1{0};    // ok
std::atomic<int> ai2(0);    // ok
std::atomic<int> ai3 = 0;   // error!

//It’s thus easy to understand why braced initialization is called “uniform.” Of
//C++’s three ways to designate an initializing expression, only braces can be
//used every‐ where.

//Brace init forbids narrowing conversions!
double x,y,z;
int i1{x+y+z};  // error!
int i1(x+y+z);  // ok!
int i1 = x+y+z; // ok!

// Drawbacks of braces:

//In constructor calls, parentheses and braces have the same meaning as long as
//std::initializer_list parameters are not involved:
class Widget {
    public:
        Widget(int i, bool b);
        Widget(int i, double d);
};
Widget w1(10, true);    // calls first ctor
Widget w2{10, true};    // also calls first ctor
Widget w3(10, 5.0);     // calls second ctor
Widget w4{10, 5.0};     // also calls second ctor

//If, however, one or more constructors declare a parameter of type
//std::initializer_list, calls using the braced initialization syntax STRONGLY
//prefer the overloads taking std::initializer_lists.

class Widget {
    public:
        Widget(int i, bool b);
        Widget(int i, double d);
        Widget(std::initializer_list<long double> il);
};
Widget w1(10, true);    // calls first ctor
Widget w2{10, true};    // converts 10 and true to long double, calls 3rd ctor.
Widget w3(10, 5.0);     // calls second ctor
Widget w4{10, 5.0};     // converts 10 and 5.0 to long double, calls 3rd ctor.

// Even copy/move constructors can be hijacked by initializer list constructors:

class Widget {
    public:
        Widget(int i, bool b);
        Widget(int i, double d);
        Widget(std::initializer_list<long double> il);

        operator float() const;
};
Widget w5(w4); // uses parens, calls copy ctor
Widget w6{w4}; // uses braces, calls std::initializer_list ctor (w4 converts to
               // float, and float converts to long double)
Widget w8{std::move(w4)};   // converts and calls {} ctor again.

// Even if not possible to convert, that constructor is preferred:

class Widget {
    public:
        Widget(int i, bool b);
        Widget(int i, double d);
        Widget(std::initializer_list<bool> il); // BOOL now!
};
Widget w{10, 5.0}; // error! requires narrowing conversions

// If there are no conversions, then we fallback to normal overload resolution
// rules:

class Widget {
    public:
        Widget(int i, bool b);
        Widget(int i, double d);
        Widget(std::initializer_list<std::string> il);
};

Widget w1(10, true);    // uses parens, calls first ctor
Widget w2{10, true};    // uses braces, calls first ctor
Widget w3(10, 5.0);     // uses parens, calls second ctor
Widget w4{10, 5.0};     // uses braces, calls second ctor

// For empty braces, it doesn't think it is an empty list:

Widget w1;      // calls default ctor
Widget w2{};    // calls default ctor
Widget w3();    // most vexing parse! declares a function!
Widget w4({});  // calls std::initializer_list ctor with empty list
Widget w4{{}};  // calls std::initializer_list ctor with empty list

// Since std::vector has a initializer list constructor, notice the VERY big
// difference between the lines:
std::vector<int> v1(10, 20);
std::vector<int> v2{10, 20};

// Consider the code:
template<typename T,
typename... Ts>
void doSomeWork(Ts&&... params)
{
    // create local T object from params... :
    // Which of the two forms should be used?
    T localObject(std::forward<Ts>(params)...); // using parens
    T localObject{std::forward<Ts>(params)...}; // using braces
}

// If T has an intializer list constructor, those two lines have different
// meanings.
// Which is correct? The author of doSomeWork can’t know. Only the caller can.

// This is precisely the problem faced by the Standard Library functions
// std::make_unique and std::make_shared. These functions resolve the problem
// by internally using parentheses and by documenting this decision as part of
// their interfaces.
