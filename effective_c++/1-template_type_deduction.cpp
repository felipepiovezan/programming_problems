//See item 4.
template<typename T> class printer;

//Template type deduction:

//template<typename T>
//void f(ParamType param);
//
//f(expr);

// Case 1: ParamType is a non-universal-Reference or Pointer.
//
// 1. If expr’s type is a reference, ignore the reference part.
// 2. Then pattern-match expr’s type against ParamType to determine T.⇧

template<typename T>
void foo1(T &param) {
    printer<T> T_printer;
}

//void part1() {
//    int x = 17;
//    foo1(x);         // T is int, param is int&
//    int &ref = x; 
//    foo1(ref);       // T is int, param is int&
//    const int &constref = x;
//    foo1(constref);  // T const int, param is const int&
//}

template<typename T>
void foo2(const T &param){
    printer<T> T_printer;
}

//void part2() {
//    int x = 17;
//    foo2(x);         // T is int, param is const int&
//    int &ref = x; 
//    foo2(ref);       // T is int, param is const int&
//    const int &constref = x;
//    foo2(constref);  // T int, param is const int&
//}

// Case 2: ParamType is a universal reference.
//
// 1. If expr is an rvalue, Case 1 rules apply.
// 2. If expr is an lvalue, both T and ParamType are lvalue references.

template<typename T>
void foo3(T &&param) {
    printer<T> T_printer;
}

//void part3() {
//    int x = 17;
//    foo3(x);            // T is int&, param is int&.
//    int &ref = x;
//    foo3(ref);          // T is int&, param is int&.
//    const int &constref = x;
//    foo3(constref);     //T is const int&, param is const int&.
//    foo3(17);           //T is int, param is int&&.
//}

// Case 3: ParamType is not a reference nor a pointer.
//
// 1. If expr is a reference, ignore the reference.
// 2. If, after ignoring the reference, expr is const/volatile,
//    ignore const/volatile too.
// We're passing a copy, so it makes sense to drop those.

template<typename T>
void foo4(T param) {
    printer<T> T_printer;
}

void part4() {
    int x = 27;
    const int const_int = x;
    const int& constref = x;
    foo4(x);               // T's and param's types are both int
    foo4(const_int);       // T's and param's types are both int
    foo4(constref);        // T's and param's types are both int

    const char* const ptr = "Fun with pointers";
    foo4(ptr);             //T and param's types are both const char *.
}
