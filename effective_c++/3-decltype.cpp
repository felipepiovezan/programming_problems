
const int i = 0; // decltype(i) is const int

bool f(const Widget& w); // decltype(w) is const Widget&
                         // decltype(f) is bool(const Widget&)
Widget w; // decltype(w) is Widget
if (f(w)) ... // decltype(f(w)) is bool

struct Point {
    int x, y;
};
// decltype(Point::x) is int
// decltype(Point::y) is int

template<typename T>
class vector {
    public:
        T& operator[](std::size_t index);
};
vector<int> v;
// decltype(v) is vector<int>
// decltype(v[0]) is int&

template<typename Container, typename Index>
auto authAndAccess(Container& c, Index i) -> decltype(c[i])
{
    authenticateUser();
    return c[i];
}
//Works in C++. But we'd like to be able to use both lvalues and rvalues as the
//container with this function. This won't work with non-const reference, as
//rvalues can't bind to them.
//Fix is to use a universal reference (C++11 final version):

template<typename Container, typename Index>
auto authAndAccess(Container&& c, Index i) ->
    decltype(std::foward<Container>(c)[i])
{
    authenticateUser();
    return std::foward<Container>(c)[i];
}

//With C++14, we can use auto type deduction:

template<typename Container, typename Index>
auto authAndAccess(Container& c, Index i)
{
    authenticateUser();
    return c[i]; 
}
// But notice that auto type deduction here will discard the & reference
// that most [] operators use. We want to use this function both when
// [] returns a reference and when it doesn't, so using auto& wouldn't work,
// as we could be returning a reference to a temporary if [] returns a copy!

//We need to return *exactly* what [] returns:

template<typename Container, typename Index>
decltype(auto) authAndAccess(Container& c, Index i)
{
    authenticateUser();
    return c[i];
}

// decltype(auto) also works in other situations where you want to avoid
// auto type deduction:

Widget w;
const Widget& cw = w;
auto myWidget1 = cw; // auto type deduction: type is Widget
decltype(auto) myWidget2 = cw; // type is const Widget&

// Finally, we fix the issue with universal references (C++14 final version):

template<typename Container, typename Index>
decltype(auto) authAndAccess(Container&& c, Index i)
{
    authenticateUser();
    return std::forward<Container>(c)[i];
}

// Decltype weird behavior:
// Applying decltype to a name yields the declared type for that name. Names
// are lvalue expressions, but that doesn’t affect decltype’s behavior. For
// lvalue expressions more complicated than names, however, decltype ensures
// that the type reported is always an lvalue reference. That is, if an lvalue
// expression other than a name has type T, decltype reports that type as T&.

decltype(auto) f1()
{
    int x = 0;
    return x; // decltype(x) is int, so f1 returns int
}
decltype(auto) f2()
{
    int x = 0;
    return (x); // decltype((x)) is int&, so f2 returns int&
}
