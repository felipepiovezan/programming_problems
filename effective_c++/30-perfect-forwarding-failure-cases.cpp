// “Forwarding” just means that one function passes—forwards —its parameters to
// another function. The goal is for the second function (the one being
// forwarded to) to receive the same objects that the first function (the one
// doing the forwarding) received.

// That rules out by-value parameters, because they’re copies of what the
// original caller passed in. We want the forwarded-to function to be able to
// work with the originally-passed-in objects. Pointer parameters are also
// ruled out, because we don’t want to force callers to pass pointers.

// Perfect forwarding means we don’t just forward objects, we also forward
// their salient characteristics: their types, whether they’re lvalues or
// rvalues, and whether they’re const or volatile.

// Simple version of fwd:
template <typename T>
void fwd(T&& param) {
  f(std::forward<T>(param));
}

template <typename... Ts>
void fwd(Ts&&... params) {
  f(std::forward<T>(params)...);
}

// We say that perfect forwarding fails if calling f with some argument does
// something different than calling fwd with the same argument:

f(expression);
fwd(expression);  //should do the same as above.

// Case 1: initializer lists are forbidden from being deduced in certain
// contexts.
 
// Braced initializers are forbidden from having their type deduced in a function
// call. It is a "non-deduced context".

fwd({1,2,3});  // fails to compile.

// However, using auto they can be deduced:
auto list = {1,2,3};  //list is a std::initializer_list
fwd(list);  // ok, T is deduced correctly.

// Case 2: 0 or NULL as null pointers.

fwd(NULL);  //deduced to be int.

// Fix: use nullptr.

// Case 3: Declaration only integral static const data members.

class Widget {
  public:
    static const std::size_t MinVals = 28; // MinVals' declaration
};

// no defn. for MinVals
std::vector<int> widgetData;
widgetData.reserve(Widget::MinVals); // use of MinVals

// There is no need to define an integral static const member because
// compilers will perform const propagation on them (they are required to do so).

// But if the address of this variable were to be taken, then we would get a 
// link-time error stating an undefined reference to the variable.

void (std::size val);
f(Widget::MinVals); // ok! treated as f(28);

fwd(Widget::MinVals); // error! fwd receives a reference, thus the address of 
                      // the variable is taken.

// Not all compilers enforce this restriction, though, so it might compile. But
// to make the code portable, define the variable too!
const std::size_t Widget::MinVals; // in Widget's .cpp file

// No need to = 28; again.

// Case 4: Overloaded function names or template names.

// If f is:
void f (int (*pf)(int));
void f (int pf(int));  // either one is fine.

// And we have an overloaded function:
int process(int value);
int process(int value, int num);

// This is fine:
f(process); // Compilers know which "process" to use: the one that matches the
            // expected type.

// But this is not fine:
fwd(processVal);  // error! which processVal to use?

// Similarly:
template<typename T>
T workOnVal(T param) {}

fwd(workOnVal); // error! Which instantiation to use?

// The fix is akin to the auto fix.
using FuncType = int (*)(int);
FuncType FuncPtr = processVal;

fwd(processVal);  // ok!
fwd(static_cast<FuncType>(workOnVal)); // ok!

// this requires that you know the type of function pointer that fwd is for‐
// warding to. It’s not unreasonable to assume that a perfect-forwarding
// function will document that. After all, perfect-forwarding functions are
// designed to accept any‐ thing, so if there’s no documentation telling you
// what to pass, how would you know?

// Case 5: Bitfields.

// You can't take references/pointers to bitfields.

struct IPv4Header {
  std::uint32_t version:4,
    IHL:4,
    DSCP:6,
    ECN:2,
    totalLength:16;
};

// If f is:
void f(std::size_t sz); 

// Then:
IPv4Header h;
f(h.totalLength); // fine

//But:
fwd(h.totalLength); // error! “A non-const reference shall not be bound to
                    //a bit-field.”
// (a const reference would be a reference to a copy too, not to the original
// value).

// copy bitfield value; see Item 6 for info on init. form
auto length = static_cast<std::uint16_t>(h.totalLength);
fwd(length);
