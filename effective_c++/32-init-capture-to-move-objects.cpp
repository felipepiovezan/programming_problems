// Some types cannot be copied, they can only be moved. But the C++11 features
// have no mechanism to move something into a value captured by a lambda.
//
// "init capture" is a mechanism allowing us to specify:
// 1- the name of the data member in the closure class,
// 2- an expression initializing the data member.

class Widget {
public:
...
private:
...
};

auto pw = std::make_unique<Widget>();

auto func = [pw = std::move(pw)] {  // = make_unique would also work. Any expr!
  return pw->isValidated()
    && pw->isArchived();
};

// The scope to the left of "=" is that of the closure class. 
// The scope to the right is the initializing expression.

// In C++11, it's possible to emulate this by either:
// 1- Writing a closure class yourself or
// 2- Using std::bind.
// See the book for examples.

// !!!!!
// By default, the operator() member function inside the closure class
// generated from a lambda is const. That has the effect of rendering all data
// members in the closure const within the body of the lambda.
// To avoid that, declare the lambda mutable (after the argument list).
