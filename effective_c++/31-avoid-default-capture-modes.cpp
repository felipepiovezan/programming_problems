// lambda expression = an expression, part of the source code.
std::find_if(container.begin(), container.end(),
    [](int val) { return 0 < val && val < 10; });  // << This is the lambda.

// closure = is the runtime object created by a lambda.

// closure class = class from which a closure is instantiated. Each lambda 
// causes an unique closure class to be generated.

// Reference pitfalls:

FilterContainer filters;
void addDivisorFilter()
{
  auto calc1 = computeSomeValue1();
  auto calc2 = computeSomeValue2();
  auto divisor = computeDivisor(calc1, calc2);
  filters.emplace_back(
      [&](int value) { return value % divisor == 0; }
      );
}

// "divisor" ceases to exist as soon as addDivisorFilter returns! Using that
// filter results in undefined behavior.

// The same problem would occurr if divisor had been an explicit capture, however
// it would've been much more obvious where the error was: you literally had to
// type it.


// A default by value would fix this, but...

class Widget {
  public:
    void addFilter() const {
      filters.emplace_back(
        [=](int value) { return value % divisor == 0; } // Wrong!!
      );
    }
  private:
    int divisor;
};

// !!!!!!!!!!!!!!!!!!!!!!!!!!
// Captures apply only to non-static local variables (including parameters)
// visible in the scope where the lambda is created.

// divisor is not a local variable, it's a data member of Widget. How come
// the code compiles, then? If we remove the [=], it won't compile... This also
// won't compile:

void Widget::addFilter() const
{
  filters.emplace_back(
    [divisor](int value)
    { return value % divisor == 0; }
  );
}

// [=] is capturing the "this" pointer! The compiling [=] code is equivalent to:

void Widget::addFilter() const
{
  auto currentObjectPtr = this;
  filters.emplace_back(
    [currentObjectPtr](int value){
      return value % currentObjectPtr->divisor == 0;
  });
}

// Another pitfall is due to static/global variables. They cannot be captured,
// but they can be used inside lambdas. We know their addresses in compile time,
// so those are hardcoded and are never captured.

void addDivisorFilter()
{
  static auto calc1 = computeSomeValue1();
  static auto calc2 = computeSomeValue2();
  static auto divisor = computeDivisor(calc1, calc2);
  filters.emplace_back(
      [=](int value) //captures nothing!
      { return value % divisor == 0; }
  ); 
  ++divisor; // modify divisor
}

// All filters added by this function will use the current value of the static
// variable "divisor".
