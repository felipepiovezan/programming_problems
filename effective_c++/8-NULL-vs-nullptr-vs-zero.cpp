void f(int);
void f(bool);
void f(void*);

f(0);       // calls f(int), not f(void*)
f(NULL);    // might not compile, but typically calls f(int). Never calls
            // f(void*)
f(nullptr);  // calls f(void*)

// what is the type of result?
auto result = findRecord( /* arguments */ );
if (result == 0) {
}

// we don't know! Because NULL is an integral type.

// this, on the other hand, says that result MUST be a pointer type, otherwise
// the code won't compile.
auto result = findRecord( /* arguments */ );
if (result == nullptr) {
}

// Suppose you want to write code like this:
template<typename FuncType, typename MuxType, typename PtrType>
decltype(auto) lockAndCall(FuncType func, MuxType& mutex, PtrType ptr)
{
    MuxGuard g(mutex);
    return func(ptr);   // func receives a shared/unique pointer.
}

auto result1 = lockAndCall(f1, f1m, 0);         // error!
auto result2 = lockAndCall(f2, f2m, NULL);      // error!
auto result3 = lockAndCall(f3, f3m, nullptr);   // ok!

// If NULL or 0 are passed, template type deduction will kick in and deduce
// int as their type. This is not compatible with the pointer types. 
