
// A
void f(Widget&& param);          // rvalue reference

// B
Widget&& var1 = Widget();        // rvalue reference

// C
auto&& var2 = var1;              // not rvalue reference

// D
template<typename T>
void f(std::vector<T>&& param);  // rvalue reference

// E
template<typename T>
void f(T&& param);               // not rvalue reference

// Important to distinguish the two uses of &&:
// 1- rvalue reference
// 2- either rvalue reference or lvalue reference!
//
// In case 2, their dual nature permits them to bind to rvalues (like rvalue
// references) as well as lvalues (like lvalue references).  Furthermore, they
// can bind to const or non-const objects, to volatile or non-volatile objects,
// even to objects that are both const and volatile.

auto&& var2 = var1;              // forwarding reference.

template<typename T>
void f(T&& param);               // forwarding reference. 

// What both of those cases have in common is the presence of type deduction!
// In A, B and D, we have T&& without type deduction, so those are rvalues.
// In order to have an universal reference, we must exactly T&& type deduction,
// which is D is an rvalue.


template<typename T>
void f(T&& param);    // param is a universal reference

Widget w;
f(w);                 // lvalue passed to f; param's type is  Widget& (i.e., an
                      // lvalue reference)
f(std::move(w));      // rvalue passed to f; param's type is Widget&& (i.e., an
                      // rvalue reference)

template<typename T>
void f(std::vector<T>&& param);  // param is an rvalue reference

std::vector<int> v;
f(v);  // error! can't bind lvalue to rvalue reference

// Some corner cases:

// Even the simple presence of a const qualifier is enough to disqualify a
// reference from being universal:
template<typename T>
void f(const T&& param);    // param is an rvalue reference

template<class T, class Allocator = allocator<T>>
class vector {
  public:
    void push_back(T&& x); // This is ALSO an rvalue!
};

// Why? Because type deduction doesn't happen for that particular function, it
// happens when the class is instantiated:
class vector<Widget, allocator<Widget>> {
  public:
    void push_back(Widget&& x);
};

// Here it is evident that the type of x is an rvalue reference.
// The user has to specifically instantiate the whole class before this function
// is called, and when it is called, the type has already been deduced.

// Compare to emplace back:
template<class T, class Allocator = allocator<T>>
class vector {
  public:
    template <class... Args>
    void emplace_back(Args&&... args); // Universal reference
};

// Here, Args in independent of the vector's template argument, and Args
// must have its type deduced on every invocation.

template<typename MyTemplateType>
void someFunc(MyTemplateType&& param);

// To conclude, param is an universal reference when type deduction happens:
int x = 3;
someFunc(x);

// But not when type deduction doesn't happen:
someFunc<int>(x); //rvalue. Error! Can't bind lvalue ref to rvalue ref.
someFunc<int>(3); // rvalue, ok.

// auto variables may also denote universal references. Specially in C++14:
auto timeFuncInvocation =
  [](auto&& func, auto&&... params)
  {
    // start timer;
    std::forward<decltype(func)>(func)(
        std::forward<decltype(params)>(params)...
        );
    // stop timer and record elapsed time;
  };
