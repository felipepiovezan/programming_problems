#include <cstdint>
#include <iostream>
#include <string>

constexpr int num_bits = 32;

struct solver {
  int fx, fy;
  bool possible = false;
  std::string ans;
  std::string path;
  int ans_size;

  bool good(int x, int y) { return x == fx && y == fy; }

  bool bit(int i, int x) { return (1 << i) & x; }

  void recurse(int x, int y, int i) {
    if (good(x, y) && i < ans_size) {
      possible = true;
      ans = path;
      ans_size = i;
      return;
    }
    if (i >= num_bits || i >= ans_size)
      return;

    if (bit(i, x) == bit(i, fx) && bit(i, y) == bit(i, fy)) {
      return;
    }
    if (bit(i, x) != bit(i, fx) && bit(i, y) != bit(i, fy)) {
      return;
    }

    int delta = (1 << i);

    if (bit(i, x) != bit(i, fx)) {
      // Of these two recursions, exactly ONE will keep branching,
      // the other will immediately stop.
      // To see why: note that a feasiable solution will have the i+1th bit
      // of X exclusive or Y differing from the targets (not both, not none)
      // If the i+1th bit of X and Y are already in this good case, then we
      // HAVE to maintain it by going EAST, going WEST would make both good or
      // both bad.
      path[i] = 'E';
      recurse(x + delta, y, i + 1);
      path[i] = 'W';
      recurse(x - delta, y, i + 1);
    }
    else if (bit(i, y) != bit(i, fy)) {
      path[i] = 'N';
      recurse(x, y + delta, i + 1);
      path[i] = 'S';
      recurse(x, y - delta, i + 1);
    }
  }

  std::string solve() {
    ans_size = 33;
    ans = std::string('0', 33);
    path = std::string('0', 33);

    recurse(0, 0, 0);
    ans.resize(ans_size);

    if (possible)
      return ans;
    return "IMPOSSIBLE";
  }
};

using namespace std;

int main() {
  int n;
  cin >> n;

  for (int i = 1; i <= n; i++) {
    int x, y;
    cin >> x >> y;
    cout << "Case #" << i << ": " << solver{x, y}.solve() << "\n";
  }
}
