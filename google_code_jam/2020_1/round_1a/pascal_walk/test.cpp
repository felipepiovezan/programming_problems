#include <algorithm>
#include <iostream>
#include <vector>
#include <cassert>

constexpr auto sentinel = -1;

std::vector<std::vector<int>> get_triangle() {

  std::vector<std::vector<int>> triangle(501, std::vector<int>());
  triangle[1].push_back(sentinel);
  triangle[1].push_back(1);

  for (int row = 2; row <= 500; row++) {
    triangle[row].push_back(sentinel);
    triangle[row].push_back(1);

    for (int col = 2; col < row; col++)
      triangle[row].push_back(triangle[row - 1][col] +
                              triangle[row - 1][col - 1]);

    triangle[row].push_back(1);
  }

  return triangle;
}

static const std::vector<std::vector<int>> triangle = get_triangle();

void visit(int row, int col) {
  assert(triangle[row][col] != sentinel);
  std::cout << row << " " << col << "\n";
}

void solve(int target) {
  int newtarget = std::max(1, target - 30);

  int current_row = 1;
  int current_col = 1;
  int current_sum = 0;
  int target_bits = newtarget;

  while (current_sum < newtarget) {

    // Need the entire row
    if (target_bits & 1) {
      // there are "current_row" columns in "current_row".
      int dest_col = current_col == 1 ? current_row : 1;
      // Are we going left or right?
      int direction = current_col == 1 ? 1 : -1;

      while (true) {
        current_sum += triangle[current_row][current_col];
        visit(current_row, current_col);

        if (current_col == dest_col)
          break;

        current_col += direction;
      }
    }
    // Skip the row
    else {
      assert(current_col == 1 || current_col == current_row);
      assert(triangle[current_row][current_col] == 1);
      visit(current_row, current_col);
      current_sum++;
    }

    target_bits >>= 1;
    current_row++;
    // If we are on the right side of the triangle, the col increases with the
    // row.
    if (current_col != 1)
      current_col++;
  }

  while(current_sum < target) {
      assert(current_col == 1 || current_col == current_row);
      visit(current_row, current_col);
      current_sum++;
      current_row++;

      // If we are on the right side of the triangle, the col increases with the
      // row.
      if (current_col != 1)
        current_col++;
  }
}

int main() {
  int num_tests;
  std::cin >> num_tests;

  for (int i = 1; i <= num_tests; i++) {
    std::cout << "Case #" << i << ":\n";
    int target;
    std::cin >> target;
    solve(target);
  }
}
