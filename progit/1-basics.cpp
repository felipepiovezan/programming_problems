// Short status:
git status -s

 M README
MM Rakefile
A  lib/git.rb
M  lib/simplegit.rb
?? LICENSE.txt

??-> files not tracked
A -> new files added to the staging area.
M -> modified

Left column: status in the staging area
Right column: status on the working tree.

// Ignoring files in .gitignore:
# ignore all .a files
*.a

# but do track lib.a, even though you're ignoring .a files above
!lib.a

# only ignore the TODO file in the current directory, not subdir/TODO
/TODO

# ignore all files in the build/ directory
build/

# ignore doc/notes.txt, but not doc/server/arch.txt
doc/*.txt

# ignore all .pdf files in the doc/ directory and any of its subdirectories
doc/**/*.pdf

// Diffing changes.
  git diff
shows the changed that are still unchanged. I.e. compares the working dir with
the staging area.
  git diff --staged  (--cached is a synonym)
will compare the staged changes against the last commit.

  git difftool
will open the changes in a tool that has been previously specified.
  git difftool --tool-help

// Committing
  git commit
will commit everything in the staged area. It will open the configured editor.
  git config --global core.editor
to change the editor.

It usually adds the latest output of git status commented out in the commit.
  git commit -v
also adds the diff to the commit message.

  git commit -m "message here"

  git commit -a
automatically stage every file that is already tracked before doing the commit,
letting you skip the git add part.

// Removing files
If you simply rm a file, it will show up as unstaged. (you can still git rm 
later)
  git rm <filename>
removes the file from the disk AND stages that change.

If you don't want Git to track the file anymore (remove it from the staging
area) but keep it on the disk:
  git rm --cached <filename>

// Moving files:
  git mv <source> <destination>
Is usually equivalent to
  mv <source> <destination>
  git rm <source>
  git add <destination>
Git figures out that this is a rename.

// Viewing commit history:
  git log
lists the commits made in that repository in reverse chronological order — that
is, the most recent commits show up first.

There are many popular options here:

  git log -p -<Number>
-p means --patch and it will show the difference (the "patch" cmd output)
introduced in each commit.
-<Number> limits the number of log entries displayed.

  git log --stat
will show the abbreviated stats for each commit (e.g. number of lines changed
per file)

  git log --pretty=<oneline,short,full,fuller>
will display commits with varying degrees of information

For full control of the output, we can use format:
  git log --pretty=format"<format>"
  git log --pretty=format"%h - %an, %ar : %s"
The last command prints:
  // ca82a6d - Scott Chacon, 6 years ago : changed the version number
  // 085bb3b - Scott Chacon, 6 years ago : removed unnecessary test
  // a11bef0 - Scott Chacon, 6 years ago : first commit

The --graph option is very useful when used with oneline/format:
  git log --pretty=format:"%h %s" --graph
Will print:
    * 2d3acf9 ignore errors from SIGCHLD on trap
    *  5e3ee11 Merge branch 'master' of git://github.com/dustin/grit
    |\
    | * 420eac9 Added a method for getting the current branch.
    * | 30e367c timeout code and tests
    * | 5a09431 add timeout protection to grit
    * | e1193f8 support for heads with slashes in them
    |/
    * d6016bc require time for xmlschema
    *  11d191e Merge branch 'defunkt' into local

Other time-limiting options:
  git log --since=2.weeks
  git log --grep=\*apple\*

There is also the "pickaxe" option, which shows commits that changed the number
of occurrences of a string:
  git log -S function_name

Similarly, we can find commits touching a particular folder/file:
  git log -- my_folder/
  git log -- my_folder/test.cpp

An example using a lot of the concepts:
  git log --pretty="%h - %s" --author=gitster --since="2008-10-01" \
     --before="2008-11-01" --no-merges -- t/
This is printing commits on one line, with format hash - subject, authored
by gitster, since and before those dates, ignoring merges, and only commits
that touched files inside folder t/

// Fixing the latest commit
  git commit --amend
will replace the latest commit with a new commit: combining the latest commit
with the staging area. It will give you a chance to update the commit message.

// Useful for the next topics.
Here is the output of git status:

    On branch master
    Changes to be committed:
      (use "git reset HEAD <file>..." to unstage)

        renamed:    README.md -> README

    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   CONTRIBUTING.md

// Unstaging a staged file
  git reset HEAD <staged_file>
!!!! Notice that it is super important that the <staged_file> is not modified in
the working directory too!
This is the command suggested by git status.

// Unmodifying a modified file:
If you want to discard your changes in the working directory:
  git checkout -- CONTRIBUTING.md
!!!! This is a super dangerous command too: git will simply overwrite the file.

Pretty much everything that was commited can be recovered, but if you DON'T
commit, then it is certain to be lost.

// Working with remotes:
  git remote
Will list the shortnames for each remote handle.
  git remote -v 
Will also show the URLs associated with the remote.
  git remote show origin
Will show each branch available, which ones are tracked and what pushes to what.

  git remote add <shortname> <url>
Add a remote which can then be referred to by <shortname>
When cloning a repo, "origin" becomes the name of that remote.

  fit fetch <remote_shortname>
Will get all the data that you currently don't have from that remote.

When fetching from origin, it doesn't merge to your local branch.
By default, git clone sets up the local master branch to *track* the remote
master branch. You can then use git pull to do a fetch followed by a merge with
origin/master.

  git push <remote> <branch>
  git push origin   master

// Tagging
  git tag
lists the available tags.

Tags can be:
  - lightweight: they are just a pointer to a specific commit.
  - Annotated: checksummed, tagger name, email, date, signed/verified.

  git tag -a <tagname> [-m "<tagging message"]
creates an annotated tag.
  git tag <tagname>
creates a lightweight tag.

  git push origin <tagname>
Tagnames are not pushed by default, it must be done explicitly.

  git checkout <tagname>
Will put you in a deatched HEAD state. Any commits here won't belong to any
branch and will be unreachable, except by hash. You might want to create a
branch:
  git checkout -b <branchname> <tagname>

// Git aliases:
  git config --global alias.unstage 'reset HEAD --'
Will make the following two commands equivalent:
  git unstage fileA
  git reset HEAD -- fileA

  git config --global alias.last 'log -1 HEAD'
Creates a command to see the last commit.
