When you commit, an object is created that contains:
  1- a pointer to the snapshot of the content you just staged.
  2- name/email/message.
  3- pointer to the commit(s) that directy came before this commit. (zero for
      init commit)

When you stage:
  1- git computes the checksum for each file.
  2- git stores that version of each file in the repo. That is called a blob.

When you commit:
  1- checksums each subdirectory
  2- stores those "tree" objects in the repo.
  3- the commit also has a pointer to the root project tree.

A branch is just a pointer to a commit.

  git branch <branch_name>
creates a new pointer to the same commit we are currently on.

Special pointer HEAD: point to the local branch we are currently on.

  git log --oneline --decorate
Shows where branch pointers are pointing. Example:

f30ab (HEAD -> master, testing) add feature #32 - ability to add new formats to the central interface
34ac2 Fixed bug #1328 - stack overflow under certain conditions
98ca9 The initial commit of my project

  git checkout <branch_name>
moves HEAD to branch_name. It also reverts files as appropriate to restore them
to their correct state in the commit pointed to by the branch.

  git log --oneline --decorate --graph --all
Will show the history of all commits and how they diverged. Example:

* c2b9e (HEAD, master) made other changes
| * 87ab2 (testing) made a change
|/
* f30ab add feature #32 - ability to add new formats to the
* 34ac2 fixed bug #1328 - stack overflow under certain conditions
* 98ca9 initial commit of my project

Commits:
============

If your development history has diverged from some older point. Because the
commit on the branch you’re on isn’t a direct ancestor of the branch you’re
merging in, Git has to do some work. In this case, Git does a simple three-way
merge, using the two snapshots pointed to by the branch tips and the common
ancestor of the two.

Occasionally, this process doesn’t go smoothly.
Suppose you are on master and you try 
  git merge iss53
Open the file with conflict.

<<<<<<< HEAD:index.html
<div id="footer">contact : email.support@github.com</div>
=======
<div id="footer">
 please contact us at support@github.com
</div>
>>>>>>> iss53:index.html

the version in HEAD (your master branch, because that was what you had checked
out when you ran your merge command) is the top part of that block
(everything above the =======)

the version in your iss53 branch looks like everything in the bottom part.

Branch management:
==============
  git branch
will list all branches available.

  git branch -v
will show you the last commit on each branch.

  git branch --merged
will tel you the branches that have already been merged into your current branch.
Example:

$ git branch --merged
  iss53
* master

In this case, it is safe to
  git branch -d iss53

Because all the work has been incorporated into the current branch, and nothing
will be lost

  git branch --no-merged
Shows all branches containing work not present in the current branch.
Trying to delete any branches returned by the command above will result in an 
error, because it would result in data loss. You can force it with -D.

You can also query information about some other branch without checking it out
first:
   git checkout testing
   git branch --no-merged master
