Main areas:
  .git directory (repository)
  working directory (the files that we see)
  staging area (contained inside the .git dir, things that will go in the next
                commit)

git config files can be configured with the
  git config
command. Use: --system, --global or --local (default) to configure the
following files:

1- /etc/gitconfig  --system 
2- ~/.gitconfig    --global
3- .git/config     --local  (this is per project)

Each level overrides the previous one.

git config --list
  will show the values git has read.

git config --show-origin <config_name>
  will show which of the files had a final say in that config's value.
