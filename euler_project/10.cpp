#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

std::vector<bool> sieve(int cap)
{
    std::vector<bool> ans(cap, true);
    ans[0] = false;
    ans[1] = false;

    int p = 2;
    while (p < cap)
    {
        for(int i = 2*p; i < cap; i += p)
        {
            ans[i] = false;
        }
        
        p++;
        while (p < cap && !ans[p])
        {
            p++;
        }
    }

    return ans;
}

int main()
{
    int cap = 2000001;
    auto vec = sieve(cap);
    auto max_p = vec.rend() - std::find(vec.rbegin(), vec.rend(), true) - 1;


    uint64_t sum = 0;
    for (int i = 0; i < cap; i++)
    {
       if (vec[i])
           sum += i;
    }

    std::cout << sum;

    return 0;
}
