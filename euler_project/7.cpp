#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

bool is_prime(int n)
{
    if (n == 1)
    {
        return false;
    }

    for (int i = 2; i*i <= n; i++)
    {
        if (n % i == 0)
            return false;
    }

    return true;
}


int main()
{
    int nth = 10001;
    int x = 1;

    for (int i = 0; i < nth; i++)
    {
        x++;

        while( !is_prime(x))
        {
            x++;
        }
        std::cout << x << std::endl;
    }

    std::cout << x << std::endl;
    return 0;
}
