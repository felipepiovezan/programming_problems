#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

uint64_t num_digits_nth_fib(uint64_t n)
{
    constexpr double log10phi = 0.208987640249978733769272089237555416822459239918210953539;
    constexpr double log105 = 0.698970004336018804786261105275506973231810118537891458689; 

    return n * log10phi - log105/2 + 1;
}

int main()
{
    for (uint64_t i = 1; i < 13; i++)
        std::cout << "(i, num digfits) = " << i << ", " << num_digits_nth_fib(i) << std::endl;

    uint64_t i = 1;
    while (num_digits_nth_fib(i) < 1000)
    {
        i++;
    }

    std::cout << "(i, num digfits) = " << i << ", " << num_digits_nth_fib(i) << std::endl;
    
    return 0;
}
