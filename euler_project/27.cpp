#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

bool is_prime(int64_t n)
{
    if (n <= 1)
    {
        return false;
    }

    for (uint64_t i = 2; i*i <= n; i++)
    {
        if (n % i == 0)
        {
            return false;
        }
    }

    return true;
}

int64_t analize_quadratics(int64_t abs_a, int64_t abs_b)
{
    std::map<int64_t, bool> prime_cache;
    int64_t max_a = -abs_a;
    int64_t max_b = -abs_b;
    uint64_t max_consec = 0;

    for (int64_t a = -abs_a + 1; a < abs_a; a++)
    {
        for (int64_t b = -abs_b + 1; b < abs_b; b++)
        {
            uint64_t num_primes = 0;

            for (uint64_t n = 0;; n++)
            {
                int64_t p = n*n + a*n + b;
                if (p < 0)
                {
                    break;
                }

                if (prime_cache.find(p) == prime_cache.end())
                {
                    prime_cache[p] = is_prime(p);
                }

                if (prime_cache[p] == false)
                {
                    break;
                }

                num_primes++;
            }

            if (num_primes > max_consec)
            {
                max_a = a;
                max_b = b;
                max_consec = num_primes;
            }
        }
    }

    return max_a * max_b;
}

int main()
{
    std::cout << analize_quadratics(1000, 1001) << std::endl;
    return 0;
}
