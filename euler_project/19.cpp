#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>

uint32_t get_days_month(uint32_t month, uint32_t year)
{
    static const std::map<uint32_t, uint32_t> map = 
    {
        {1, 31},
        {2, 28},
        {3, 31},
        {4, 30},
        {5, 31},
        {6, 30},
        {7, 31},
        {8, 31},
        {9, 30},
        {10, 31},
        {11, 30},
        {12, 31},
    };

    if ( month == 2 &&
         year % 4 == 0 &&
         (year % 100 != 0 || year % 400 == 0)
       )
    {
        return 29;
    }

    return map.at(month);
}

/* Sunday = 0
 * Monday = 1 
 * ...
 * Saturday = 6
 */
uint32_t calculate_sundays_on_first(uint32_t start_year, uint32_t end_year, uint32_t start_weekday)
{
    uint32_t ans = 0;
    
    while (start_year <= end_year)
    {
        for (uint32_t month = 1; month <= 12; month++)
        {
            std::cout << month << ", " << start_year << " = " << start_weekday << std::endl;
            if (start_weekday == 0)
            {
                ans++;
            }

            uint32_t days_month = get_days_month(month, start_year);
            start_weekday = (start_weekday + days_month) % 7;
        }

        start_year++;
    }

    return ans;
}

int main()
{
   /*January 1st 1901 was Tuesday*/
    std::cout << calculate_sundays_on_first(1901, 2000, 2) << std::endl;

    return 0;
}
