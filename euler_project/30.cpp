#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>
/*
 * If S is the sum of the digits of x each to the power of K, then
 *  S <= (10^K log x)  + 10^K
 *
 * Now we want x <= S and test those values.
 *  x <= (10^K log x)  + 10^K
 *
 * For K=5, that yields:
 * x <= 1.53 * 10^6.
 * */

template <uint64_t exp>
uint64_t pow(uint64_t x)
{
    uint64_t ans = 1;
    for (uint64_t i = 0; i < exp; i++)
    {
        ans *= x;
    }

    return ans;
}

template <uint64_t exp>
uint64_t sum_pow_digits(uint64_t x)
{
    uint64_t sum = 0;

    while (x > 0)
    {
        uint64_t digit = x % 10;
        x = x/10;
        sum += pow<exp>(digit);
    }

    return sum;
}

int main()
{
    uint64_t ans = 0;
    for (uint64_t i = 10; i <= 1530000; i++)
    {
        auto sum = sum_pow_digits<5>(i);
        if (sum == i)
        {
            std::cout << i << std::endl;
            ans += i;
        }
    }

    std::cout << "sum = " << ans << std::endl;
    return 0;
}
