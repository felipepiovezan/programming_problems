#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

uint32_t count_ways(const std::vector<uint32_t> &coins, uint32_t target, uint32_t index)
{
    if (target == 0 || index == coins.size()-1)
    {
        return 1;
    }

    uint32_t ans = count_ways(coins, target, index+1);

    while (target >= coins[index])
    {
        target -= coins[index];
        ans+= count_ways(coins, target, index+1);
    }

    return ans;
}


int main()
{
    std::vector<uint32_t> coins = {200, 100, 50, 20, 10, 5, 2, 1};
    uint32_t target = 200;

    auto ans = count_ways(coins, target, 0);

    std::cout << "final = " << ans << std::endl;
    return 0;
}
