#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

uint32_t factorial(uint32_t n)
{
    uint32_t ans = 1;
    for (uint32_t i = 2; i <= n; i++)
    {
        ans *= i;
    }

    return ans;
}

std::vector<uint32_t> get_nth_permutation(uint32_t n)
{
    n--; //0 based;

    std::vector<uint32_t> digits = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    //std::vector<uint32_t> digits = {0, 1, 2};
    std::vector<uint32_t> permutation;

    uint32_t base = 0;
    uint32_t original_size = digits.size();

    for (uint32_t i = 0; i < original_size; i++)
    {
        for (uint32_t j = 1; j < digits.size(); j++)
        {
            uint32_t delta = j * factorial((digits.size()-1));
            //std::cout << "dekta = " << delta << std::endl;
            //std::cout << "base  = " << base  << std::endl;
            if (base + delta > n)
            {
                permutation.push_back(digits[j-1]);
                digits.erase(digits.begin() + j - 1);
                base += (j-1) * factorial((digits.size()));
                break;
            }

            else if (j == digits.size() - 1)
            {
                permutation.push_back(digits[j]);
                digits.erase(digits.begin() + j);
                base += (j) * factorial((digits.size()));
                break;
            }
        }
    }

    permutation.push_back(digits.front());

    return permutation;
}

int main()
{
    for (int i = 1; i <= 6; i++)
    {
        auto ans = get_nth_permutation(i);
        std::cout << i << "-th permutation = ";
        for (auto x : ans)
        {
            std::cout << x;
        }
        std::cout << std::endl;
    }

    std::cout << "1000th permutation = ";
    auto ans = get_nth_permutation(1000000);
    for (auto x : ans)
    {
        std::cout << x;
    }
    std::cout << std::endl;

    return 0;
}
