#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

int sum(int n)
{
    return (n * (n+1))/2;
}

int sum_squares(int n)
{
    return (n)*(n+1)*(2*n+1)/6;
}

int main()
{
    int num = 100;
    std::cout << sum_squares(num) << std::endl;
    std::cout << sum(num)*sum(num) << std::endl;
    std::cout << sum_squares(num) - sum(num)*sum(num) << std::endl;
    return 0;
}
