#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

std::vector<uint32_t> get_divisors(uint32_t n)
{
    std::vector<uint32_t> divs;

    for(uint32_t d = 1; d <= n/2; d++)
    {
        if (n % d == 0)
        {
            divs.push_back(d);
        }
    }

    return divs;
}

uint32_t d(uint32_t x)
{
    auto divs = get_divisors(x);
    uint32_t init = 0;

    return std::accumulate(divs.begin(), divs.end(), init);
}

int main()
{
    uint32_t limit = 10001;
    std::set<uint32_t> amicable;
    
    for (uint32_t i = 2; i < limit; i++)
    {
         uint32_t d_i = d(i);
         uint32_t d_d_i = d(d_i);

         if (d_d_i == i && i != d_i)
         {
             amicable.insert(i);
             amicable.insert(d_i);
             std::cout << i << ", " << d_i << std::endl;
         }
    }

    std::cout << std::accumulate(amicable.begin(), amicable.end(), 0) << std::endl;

    return 0;
}
