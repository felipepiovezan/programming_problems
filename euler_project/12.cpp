#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

uint64_t get_nth_triangle_number(uint64_t n)
{
    return n*(n+1)/2;
}

uint64_t count_divisors(uint64_t n)
{
    uint64_t count = 1;
    if (n > 1) count = 2;

    for (uint64_t div = 2; div <= n/2; div++)
    {
        if (n % div == 0)
        {
            count++;
        }
    }

    return count;
}

int main()
{
    uint64_t cap = 500;
    uint64_t x = 1;

    while ( count_divisors(
                get_nth_triangle_number(x)) <= cap)
    {
        if(x%1000 == 0)
            std::cout << x << ", ," << count_divisors(get_nth_triangle_number(x)) << std::endl;
        x++;
    }

    std::cout << x << ", " << get_nth_triangle_number(x)
        << ", " << count_divisors(get_nth_triangle_number(x)) << std::endl;


    return 0;
}
