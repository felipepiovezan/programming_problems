#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

bool is_pythagorean(int a, int b, int c)
{
    return  a*a + b*b == c*c;
}

int main()
{
    int sum_to = 1000;

    for (int a = 1; a <= sum_to; a++)
    {
        for (int b = a + 1; b <= sum_to; b++)
        {
            int c = sum_to - a - b;

            if (c > 0 && is_pythagorean(a,b,c))
            {
                std::cout << a << ", " << b << ", " << c << std::endl;
                std::cout << a*b*c << std::endl;
            }
        }
    }

    return 0;
}
