#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

uint32_t gcd (uint32_t a, uint32_t b)
{
    if (b == 0)
    {
        return a;
    }

    return gcd(b, a % b);
}

int main()
{
    uint32_t num=1;
    uint32_t den=1;

    for (uint32_t x = 11; x < 100; x++)
    {
        uint32_t x1 = x % 10;
        uint32_t x2 = (x/10) % 10;

        for (uint32_t y = 10; y < x; y++)
        {
            uint32_t y1 = y % 10;
            uint32_t y2 = (y/10) % 10;

            uint32_t div1 = gcd(x,y);
            uint32_t num1 = y / div1;
            uint32_t den1 = x / div1;

            bool match = false;

            if ( y2 == x2 || y2 == x1)
            {
                uint32_t num2 = y1;
                uint32_t den2 = y2 == x2? x1 : x2;

                uint32_t div2 = gcd(num2, den2);
                num2 = num2 / div2;
                den2 = den2 / div2;

                if (num1 == num2 && den1 == den2)
                {
                    match = true;
                }
            }
            else if ((y1 == x1 && y1 != 0) || y1 == x2)
            {
                uint32_t num2 = y2;
                uint32_t den2 = y1 == x1? x2 : x1;

                uint32_t div2 = gcd(num2, den2);
                num2 = num2 / div2;
                den2 = den2 / div2;

                if (num1 == num2 && den1 == den2)
                {
                    match = true;
                }
            }

            if (match)
            {
                std::cout << y << " / " << x << std::endl;
                num *= y;
                den *= x;
            }
        }
    }

    uint32_t div = gcd(num, den);
    std::cout << den/div << std::endl;
    
    return 0;
}
