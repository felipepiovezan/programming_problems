#include <iostream>
#include <string>
#include <vector>
#include <algorithm>


void multiply(std::vector<uint32_t> &vec, uint32_t b)
{
    uint32_t carry = 0;

    for(uint32_t j = 0; j < vec.size(); j++)
    {
        vec[j] = b * vec[j] + carry;
        carry = vec[j]/10;
        vec[j] = vec[j] % 10;
    }
    while (carry > 0)
    {
        vec.push_back(carry % 10);
        carry = carry / 10;
    }
}


std::vector<uint32_t> factorial(uint32_t n)
{
    std::vector<uint32_t> fac = {1};
    for (uint32_t i = 2; i <= n; i++)
    {
        multiply(fac, i);

        for(auto x = fac.rbegin(); x != fac.rend(); x++)
            std::cout << (int) *x;
        std::cout << std::endl;
    }

    return fac;
}

int main()
{
    auto x = factorial(100);

    uint32_t sum = 0;
    for (auto d : x)
    {
        sum += d;
    }

    std::cout << "sum = " << sum << std::endl;

    return 0;
}
