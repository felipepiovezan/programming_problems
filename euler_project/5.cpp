#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

int gcd(int a, int b)
{
    if (b == 0)
    {
        return a;
    }

    return gcd(b, a%b);
}

int lcm(int a, int b)
{
    return (a/gcd(a,b))*b;
}

int main()
{
    std::vector<int> numbers;
    for(int i = 1; i <= 20; i++)
    {
        numbers.push_back(i);
    }

    int ans = 1;

    for (auto x : numbers)
    {
        ans = lcm(ans, x);
        std::cout << ans << std::endl;
    }


    std::cout << ans << std::endl;
    return 0;
}
