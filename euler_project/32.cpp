#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

bool insert_unique_digits(std::set<uint64_t> &set, uint64_t x)
{
    uint64_t original = x;
    std::set<uint64_t> no_repeats;
    while (x)
    {
        uint64_t digit = x % 10;
        if (digit == 0)
        {
            return false;
        }

        if (set.find(digit) != set.end())
        {
            return false;
        }
        
        if (no_repeats.find(digit) != no_repeats.end())
        {
            return false;
        }
        no_repeats.insert(digit);
        x = x / 10;
    }

    set.insert(no_repeats.begin(), no_repeats.end());

    return true;
}

void remove_digits(std::set<uint64_t> &set, uint64_t x)
{
    while (x)
    {
        uint64_t digit = x%10;
        set.erase(digit);
        x = x/10;
    }
}

uint64_t pow(uint64_t base, uint64_t exp)
{
    uint64_t ans = 1;
    for (uint64_t i = 0; i < exp; i++)
        ans *= base;

    return ans;
}

uint64_t log10(uint64_t x)
{
    uint64_t ans = 0;
    while (x/10)
    {
        ans++;
        x = x / 10;
    }

    return ans;
}

int main()
{
    std::set<uint64_t> nums;
    for (uint64_t x = 10; x < 99999; x++)
    {
        std::set<uint64_t> set;

        if (insert_unique_digits(set, x))
        {
            for (uint64_t y = 1; y <= x; y++)
            {   
                if (insert_unique_digits(set, y))
                {
                    uint64_t mul = x*y;
                    
                    if (insert_unique_digits(set, mul))
                    {
                        if (x == 186 && y == 39)
                            std::cout << "HERE " << mul << std::endl;

                       if(set.size() == 9)
                       {
                           std::cout << x << " * " << y << " = " << mul << std::endl;
                           nums.insert(mul);
                       }

                       remove_digits(set, mul);
                    }

                    remove_digits(set, y);
                }
            }
        }
    }

    std::cout << std::accumulate(nums.begin(), nums.end(), uint64_t{0}) << std::endl;

    return 0;
}
