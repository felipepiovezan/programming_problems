#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

//If 
//      k=2^a * 5^b * n
//where n > 1 and n is not divisible by 2 or 5, then the length of the
//transient of 1/k is max(a, b), and the period equals r, where r is the
//smallest integer such that 10^r = 1 (mod n).

uint64_t get_decimal_recurring_cycle(uint64_t k)
{
    while (k % 2 == 0)
    {
        k = k / 2;
    }
    while (k % 5 == 0)
    {
        k = k / 5;
    }

    if (k == 1)
    {
        return 0;
    }

    uint64_t n = k;

    uint64_t r = 1;
    uint64_t base = 10;
    
    while (base % n != 1)
    {
        r++;
        base = (base * 10) % n;
    }

    return r;
}

int main()
{
    uint64_t max_i = 2;
    uint64_t max_cycle = 0;
    for (uint64_t i = 2; i < 1000; i++)
    {
        uint64_t cycle_size = get_decimal_recurring_cycle(i);
        std::cout << "1/" << i << " cycle = " 
            << cycle_size << std::endl;

        if (cycle_size > max_cycle)
        {
            max_i = i;
            max_cycle = cycle_size;
        }
    }

    std::cout << "max_i, max_cycle = "
        << max_i << ", " << max_cycle << std::endl;

    return 0;
}
