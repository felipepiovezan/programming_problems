#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>

std::vector<std::vector<uint64_t>> 
read_input_from_file(const std::string& filename)
{
    std::vector<std::vector<uint64_t>> ans;
    std::ifstream file(filename);
    
    for (std::string line; std::getline(file, line); )
    {
        ans.push_back(std::vector<uint64_t>());
        std::stringstream stream(line);
        uint64_t num;
        while (stream >> num)
        {
           ans.back().push_back(num);
        }
    }

    return ans;
}


uint64_t solve_max_path(std::vector<std::vector<uint64_t>> &mat)
{
    for(uint64_t i = 1; i < mat.size(); i++)
    {
        for(uint64_t j = 0; j < mat[i].size(); j++)
        {
            uint64_t max = 0;
            
            if (j > 0)
            {
                max = mat[i-1][j-1] + mat[i][j];
            }
            if (j < mat[i].size() - 1)
            {
                max = std::max(max, mat[i-1][j] + mat[i][j]);
            }

            mat[i][j] = max;
        }
    }

    auto &last = mat.back();
    uint64_t max = last[0];

    for (auto x : last)
    {
        max = std::max(max, x);
    }

    return max;
}

int main()
{
    auto vec = read_input_from_file("18.in");
    std::cout << "answer_to_18 = " << solve_max_path(vec) << std::endl;

    vec = read_input_from_file("67.in");
    std::cout << "answer_to_67 = " << solve_max_path(vec) << std::endl;

    return 0;
}
