#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

std::vector<uint32_t> sieve(uint32_t n)
{
    n++;

    std::vector<bool> list(n, true);
    list[0] = list[1] = false;
    list[2] = true;
    uint32_t p = 2;

    while (p < n)
    {
        for (uint32_t i = 2*p; i < n; i += p)
        {
            list[i] = false;
        }

        p++;
        for (; p < n && list[p] == false; p++)
        {}
    }

    std::vector<uint32_t> primes;

    for (uint32_t i = 2; i < n; i++)
    {
        if (list[i])
        {
            primes.push_back(i);
        }
    }

    return primes;
}

struct prime_factor
{
    uint32_t prime;
    uint32_t power;
};

bool operator< (const prime_factor &lhs, const prime_factor &rhs)
{
    return lhs.prime < rhs.prime ||
        (lhs.prime == rhs.prime && lhs.power < rhs.power);
}

std::vector<prime_factor> factor(uint32_t x, const std::vector<uint32_t> &primes)
{
    std::vector<prime_factor> factors;

    for (auto p : primes)
    {
        uint32_t power = 0;
        while (x % p == 0)
        {
            x = x / p;
            power++;
        }

        if (power > 0)
        {
            factors.push_back({p, power});
        }
    }
    
    return factors;
}

uint32_t count_unique(uint32_t max_a, uint32_t max_b)
{
    auto primes = sieve(max_a);
    std::set<std::vector<prime_factor>> unique_numbers;
    
    for (uint32_t a = 2; a <= max_a; a++)
    {
        auto base_factors = factor(a, primes);
        
        for (uint32_t b = 2; b <= max_b; b++)
        {
            auto a_pow_b = base_factors;
            for (auto &x : a_pow_b)
            {
                x.power *= b;
            }
            unique_numbers.insert(std::move(a_pow_b));
        }
    }

    return unique_numbers.size();
}

int main()
{
    std::cout << count_unique(1000, 1000) << std::endl;;
    return 0;
}
