#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

uint64_t count_routes(uint64_t n)
{
    n++;
    std::vector<std::vector<uint64_t>> mat{n, std::vector<uint64_t>(n, 1)};

    for(uint64_t i = 1; i < n; i++)
    {
       for (uint64_t j = 0; j < n; j++)
       {
           uint64_t count = mat[i-1][j];
           
           if (j > 0)
           {
               count += mat[i][j-1];
           }

           mat[i][j] = count;
       }
    }

    for(uint64_t i = 0; i < n; i++)
    {
       for (uint64_t j = 0; j < n; j++)
       {
           std::cout << mat[i][j] << " ";
       }
       std::cout << std::endl;
    }
    return mat[n-1][n-1];
}

int main()
{
    std::cout << count_routes(2) << std::endl;
    std::cout << count_routes(20) << std::endl;
    return 0;
}
