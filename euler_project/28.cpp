#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

uint64_t get_sum_arith_progression(uint64_t x0, uint64_t r, uint64_t n)
{
    return n*(2*x0 + (n-1)*r)/2;
}

uint64_t sum_spirals(uint64_t n)
{
    n = (n+1)/2;
    uint64_t sum = 1;
    uint64_t x0 = 1;
    uint64_t r = 0;

    for (uint64_t i = 1; i < n; i++)
    {
        r = r + 2;
        x0 += r;
        sum += get_sum_arith_progression(
                x0, r, 4);
        std::cout << sum << std::endl;
        x0 += 3*r;
    }

    return sum;
}

int main()
{
    auto x = sum_spirals(1001);
    std::cout << x << std::endl;
    return 0;
}
