#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

std::vector<std::string> list_from_file()
{
    std::vector<std::string> names;
    std::ifstream file("22.in");

    for (std::string name; std::getline(file, name, ',');)
    {
        name.pop_back();
        name.erase(name.begin());
        names.push_back(name);
    }

    return names;
}

uint64_t get_name_score(const std::string &name)
{
    uint64_t score = 0;

    for (auto x : name)
    {
        score += x - 'A' + 1;
    }

    return score;
}

int main()
{
    auto names = list_from_file();
    std::sort(names.begin(), names.end());
    uint64_t total_score = 0;
    
    for (uint32_t i = 0; i < names.size(); i++)
    {
        total_score += get_name_score(names[i]) * (i+1);
    }

    std::cout << total_score << std::endl;
    std::cout << get_name_score(names[937]) << std::endl;
    return 0;
}
