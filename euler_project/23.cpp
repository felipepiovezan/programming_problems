#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <numeric>

std::vector<uint32_t> get_proper_divisors(uint32_t n)
{
    std::vector<uint32_t> divisors;

    for (uint32_t i = 1; i <= n/2; i++)
    {
        if (n % i == 0)
        {
            divisors.push_back(i);
        }
    }

    return divisors;
}

bool is_abundant(uint32_t n)
{
    auto divisors = get_proper_divisors(n);
    uint32_t zero = 0;
    auto sum = std::accumulate(divisors.begin(), divisors.end(), zero);

    return sum > n;
}

std::vector<uint32_t> get_abundant_smaller_than(uint32_t n)
{
    std::vector<uint32_t> abundants;

    for (uint32_t i = 12; i < n; i++)
    {
        if (is_abundant(i))
        {
            abundants.push_back(i);
        }
    }

    return abundants;
}

bool can_be_written_as_sum_of_abundants(uint32_t n)
{
    static auto abundants = get_abundant_smaller_than(28123);

    if (n > 28123)
    {
        return false;
    }

    for (auto x : abundants)
    {
        if (x >= n)
        {
            return false;
        }

        uint32_t leftover = n - x;
        if (std::binary_search(abundants.begin(), abundants.end(), leftover))
        {
            return true;
        }
    }

    return false;
}

int main()
{
    uint32_t sum = 0;

    for (uint32_t i = 1; i <= 28123; i++)
    //for (uint32_t i = 1; i <= 100; i++)
    {
        if (!can_be_written_as_sum_of_abundants(i))
        {
            sum += i;
            std::cout << i << ", " << sum << std::endl;
        }
    }

    return 0;
}
