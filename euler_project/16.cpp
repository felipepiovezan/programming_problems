#include <iostream>
#include <string>
#include <vector>
#include <algorithm>


uint64_t sum_digits_pow_2(uint32_t exp)
{
    std::vector<uint8_t> vec(exp, 0);
    vec[0] = 2;
    exp--;

    uint32_t carry = 0;
    for(uint32_t j = 0; j < exp; j++)
    {
        for(uint32_t i = 0; i < exp; i++)
        {
            vec[i] = 2 * vec[i] + carry;
            carry = vec[i]/10;
            vec[i] = vec[i] % 10;
        }
    }
    uint64_t sum = 0;
    for(auto x : vec)
    {
        sum += x;
    }

    return sum;
}


int main()
{
    std::cout << sum_digits_pow_2(15) << std::endl;
    std::cout << sum_digits_pow_2(1000) << std::endl;
    return 0;
}
