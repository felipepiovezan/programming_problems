#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <limits>
#include <sstream>
#include <string>
#include <vector>
#include <map>

struct delta {
  int x;
  int y;
  bool operator<(const delta &rhs) const {
    if (x < rhs.x)
      return true;
    if (x > rhs.x)
      return false;
    return y < rhs.y;
  }
};

std::ostream &operator<<(std::ostream &os, const delta &d) {
  os << "{" << d.x << ", " << d.y << "}";
  return os;
}

std::istream &operator>>(std::istream &is, delta &d) {
  if (!is)
    return is;
  while (is.peek() == ',' || std::isspace(is.peek()))
    is.ignore();
  if (!is)
    return is;

  char op = is.get();

  std::string num_str;
  while (is && std::isalnum(is.peek())) {
    num_str.push_back(is.get());
  }

  int num = std::stoi(num_str);

  int x = 0;
  int y = 0;

  if (op == 'R')
    x = num;
  if (op == 'L')
    x = -num;
  if (op == 'U')
    y = num;
  if (op == 'D')
    y = -num;

  d = delta{x, y};

  return is;
}

int main() {
  std::string line1, line2;
  std::cin >> line1 >> line2;
  auto stream1 = std::istringstream(line1);
  auto stream2 = std::istringstream(line2);

  std::vector<delta> deltas1{std::istream_iterator<delta>(stream1),
                             std::istream_iterator<delta>()};
  std::vector<delta> deltas2{std::istream_iterator<delta>(stream2),
                             std::istream_iterator<delta>()};

  std::map<delta, int> visited;
  delta pos{0, 0};
  int total_steps = 0;
  for (auto d : deltas1) {
    delta new_pos{pos.x + d.x, pos.y + d.y};
    if (new_pos.x != pos.x) {
      assert(new_pos.y == pos.y);
      int dir = pos.x < new_pos.x ? 1 : -1;
      for (int x = pos.x + dir; x != new_pos.x + dir; x += dir) {
        total_steps++;
        visited.emplace(delta{x, pos.y}, total_steps);
      }
    } else {
      assert(new_pos.y != pos.y);
      int dir = pos.y < new_pos.y ? 1 : -1;
      for (int y = pos.y + dir; y != new_pos.y + dir; y += dir) {
        total_steps++;
        visited.emplace(delta{pos.x, y}, total_steps);
      }
    }
    pos = new_pos;
  }

  pos = {0, 0};
  int min_dist = std::numeric_limits<int>::max();
  total_steps = 0;
  for (auto d : deltas2) {
    delta new_pos{pos.x + d.x, pos.y + d.y};
    if (new_pos.x != pos.x) {
      assert(new_pos.y == pos.y);
      int dir = pos.x < new_pos.x ? 1 : -1;
      for (int x = pos.x + dir; x != new_pos.x + dir; x += dir) {
        delta midpos = {x, pos.y};
        total_steps++;
        if (visited.count(midpos)) {
          min_dist = std::min(min_dist, visited[midpos] + total_steps);
          std::cout << "Encounter due to x: " << midpos << "\n";
        }
      }
    } else {
      assert(new_pos.y != pos.y);
      int dir = pos.y < new_pos.y ? 1 : -1;
      for (int y = pos.y + dir; y != new_pos.y + dir; y += dir) {
        delta midpos = {pos.x, y};
        total_steps++;
        if (visited.count(midpos)) {
          min_dist = std::min(min_dist, visited[midpos] + total_steps);
          std::cout << "Encounter due to y: " << midpos << "\n";
        }
      }
    }
    pos = new_pos;
  }

  std::cout << min_dist << "\n";
}
