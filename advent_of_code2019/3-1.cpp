#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <limits>
#include <sstream>
#include <string>
#include <vector>

struct delta {
  int x;
  int y;
  bool operator<(const delta &rhs) const {
    if (x < rhs.x)
      return true;
    if (x > rhs.x)
      return false;
    return y < rhs.y;
  }
};

std::ostream &operator<<(std::ostream &os, const delta &d) {
  os << "{" << d.x << ", " << d.y << "}";
  return os;
}

std::istream &operator>>(std::istream &is, delta &d) {
  if (!is)
    return is;
  while (is.peek() == ',' || std::isspace(is.peek()))
    is.ignore();
  if (!is)
    return is;

  char op = is.get();

  std::string num_str;
  while (is && std::isalnum(is.peek())) {
    num_str.push_back(is.get());
  }

  int num = std::stoi(num_str);

  int x = 0;
  int y = 0;

  if (op == 'R')
    x = num;
  if (op == 'L')
    x = -num;
  if (op == 'U')
    y = num;
  if (op == 'D')
    y = -num;

  d = delta{x, y};

  return is;
}

int main() {
  std::string line1, line2;
  std::cin >> line1 >> line2;
  auto stream1 = std::istringstream(line1);
  auto stream2 = std::istringstream(line2);

  std::vector<delta> deltas1{std::istream_iterator<delta>(stream1),
                             std::istream_iterator<delta>()};
  std::vector<delta> deltas2{std::istream_iterator<delta>(stream2),
                             std::istream_iterator<delta>()};

  std::vector<delta> visited;
  delta pos{0, 0};
  for (auto d : deltas1) {
    delta new_pos{pos.x + d.x, pos.y + d.y};
    if (new_pos.x != pos.x) {
      assert(new_pos.y == pos.y);
      for (int x = std::min(pos.x, new_pos.x); x < std::max(pos.x, new_pos.x);
           x++) {
        visited.push_back({x, pos.y});
      }
    } else {
      assert(new_pos.y != pos.y);
      for (int y = std::min(pos.y, new_pos.y); y < std::max(pos.y, new_pos.y);
           y++) {
        visited.push_back({pos.x, y});
      }
    }
    pos = new_pos;
  }

  std::sort(std::begin(visited), std::end(visited));

  pos = {0, 0};
  int min_dist = std::numeric_limits<int>::max();
  for (auto d : deltas2) {
    delta new_pos{pos.x + d.x, pos.y + d.y};
    if (new_pos.x != pos.x) {
      assert(new_pos.y == pos.y);
      for (int x = std::min(pos.x, new_pos.x); x <= std::max(pos.x, new_pos.x);
           x++) {
        delta midpos = {x, pos.y};
        if (midpos.x == 0 && midpos.y == 0) continue;
        if (std::binary_search(std::begin(visited), std::end(visited),
                               midpos)) {
          min_dist =
              std::min(min_dist, std::abs(midpos.x) + std::abs(midpos.y));
          std::cout << "Encounter due to x: " << midpos << "\n";
        }
      }
    } else {
      assert(new_pos.y != pos.y);
      for (int y = std::min(pos.y, new_pos.y); y <= std::max(pos.y, new_pos.y);
           y++) {
        delta midpos = {pos.x, y};
        if (midpos.x == 0 && midpos.y == 0) continue;
        if (std::binary_search(std::begin(visited), std::end(visited),
                               midpos)) {
          min_dist =
              std::min(min_dist, std::abs(midpos.x) + std::abs(midpos.y));
          std::cout << "Encounter due to y: " << midpos << "\n";
        }
      }
    }
    pos = new_pos;
  }

  std::cout << min_dist << "\n";
}
