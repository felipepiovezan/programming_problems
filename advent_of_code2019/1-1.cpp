#include <iostream>
#include <iterator>
#include <numeric>

int main() {
  auto ans =
      std::accumulate(std::istream_iterator<int64_t>(std::cin),
                      std::istream_iterator<int64_t>(), int64_t(0),
                      [](auto curr, auto add) { return curr + (add / 3) - 2; });
  std::cout << ans << "\n";
}
