#include <iostream>
#include <iterator>
#include <numeric>

int main() {
  auto process_fuel = [](auto curr, auto add) {
    auto to_add = add / 3 - 2;
    while (to_add  > 0) {
      curr += to_add;
      to_add = to_add / 3 - 2;
    }

    return curr;
  };

  auto ans = std::accumulate(std::istream_iterator<int64_t>(std::cin),
                             std::istream_iterator<int64_t>(), int64_t(0),
                             process_fuel);
  std::cout << ans << "\n";
}
