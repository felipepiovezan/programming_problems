#include <iostream>
#include <iterator>
#include <vector>

bool try_inputs(int noun, int verb, std::vector<int64_t> program) {
  program[1] = noun;
  program[2] = verb;

  for (int i = 0; i < program.size(); i += 4) {
    auto opcode = program[i];

    if (opcode == 99)
      break;

    if (opcode != 1 && opcode != 2) {
      std::cout << "Error! \n";
      return false;
    }

    auto src1 = program[i + 1];
    auto src2 = program[i + 2];
    auto dest = program[i + 3];

    if (opcode == 1)
      program[dest] = program[src1] + program[src2];
    else
      program[dest] = program[src1] * program[src2];
  }

  return program[0] == 19690720;
}

int main() {
  std::vector<int64_t> program{std::istream_iterator<int64_t>(std::cin),
                               std::istream_iterator<int64_t>()};
  for (int i = 0; i < 100; i++) {
    for (int j = 0; j < 100; j++) {
      if (try_inputs(i, j, program)) {
        std::cout << 100 * i + j << "\n";
        return 0;
      }
    }
  }

  std::cout << "Failed\n";
}
