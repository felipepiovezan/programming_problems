#include <stdio.h>

int main()
{
    int x0, n1, n2, n3;
    while (scanf("%d %d %d %d", &x0, &n1, &n2, &n3), (x0||n1||n2||n3))
    {
        int ans = 360*3;
        int d1 = (x0 - n1) * 9;
        if (d1 < 0) d1 += 360;

        int d2 = (n2 - n1) * 9;
        if (d2 < 0) d2 += 360;

        int d3 = (n2 - n3) * 9;
        if (d3 < 0) d3 += 360;

        printf("%d\n", ans + d1 + d2 + d3);
    }
}
