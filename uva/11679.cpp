#include <iostream>
using namespace std;

int main()
{
    int banks, inputs;
    int reserves[21];
    while (cin >> banks >> inputs && (banks || inputs))
    {
        for (int i = 1; i <= banks; i++)
        {
            cin >> reserves[i];
        }
        for (int i = 0; i < inputs; i++)
        {
            int source, dest, ammount;
            cin >> source >> dest >> ammount;
            reserves[source] -= ammount;
            reserves[dest] += ammount;
        }
        char ans = 'S'; 
        for (int i = 1; i <= banks; i++)
            if (reserves[i] < 0)
                ans = 'N';

        cout << ans << endl;
    }
}
