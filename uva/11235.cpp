#include <iostream>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <limits>
#include <cassert>

struct segment_tree {
    static constexpr int OUT_OF_BOUNDS = std::numeric_limits<int>::min();

    segment_tree(const std::vector<int> &v) : array(v) {
        tree.resize(4 * array.size(), 0);
        build(1, 0, array.size() - 1);
    }
    
    int rmq(int a, int b) {
        int most_freq = rmq(1, 0, array.size() - 1, a, b);
        return count_frequency(most_freq, a, b);
    }

    private:
    int left(int node) {return 2*node;}
    int right(int node) { return 2*node +1;}

    void build(int node, int a, int b) {
        if (a == b)
            tree[node] = array[a];
        else {
            build(left(node), a, (a+b)/2);
            build(right(node), (a+b)/2 + 1, b);
            tree[node] = combine_values(tree[left(node)], tree[right(node)],
                    a, b);
        }
    }

    int count_frequency(int x, int a, int b) {
        auto pair = std::equal_range(array.begin() + a, array.begin() + b + 1,
                x);
        assert(x > OUT_OF_BOUNDS);
        return pair.second - pair.first;
    }

    int combine_values(int v1, int v2, int a, int b)
    {
        /* When we are combining two intervals [a,middle]
         * (middle, b], the possible candidates for a most frequent
         * value in the whole interval [a,b] are:
         * 1- most freq [a,middle],
         * 2- most freq (middle, b],
         * 3- array[middle],
         */
        assert(v1 > OUT_OF_BOUNDS);
        assert(v2 > OUT_OF_BOUNDS);
        int count1 = count_frequency(v1, a, b);
        int count2 = count_frequency(v2, a, b);
        int v3 = array[(a+b)/2];
        int count3 = count_frequency(v3, a, b);

        if (count1 >= count2 && count1 >= count3)
            return v1;
        if (count2 >= count1 && count2 >= count3)
            return v2;
        return v3;
    }

    int rmq(int node, int visit_a, int visit_b, int want_a, int want_b) {
        if (want_b < visit_a || want_a > visit_b)
            return OUT_OF_BOUNDS;
        if (visit_a >= want_a && visit_b <= want_b)
            return tree[node];

        int partial1 = rmq(left(node), visit_a, (visit_a + visit_b)/2, want_a,
                want_b);
        int partial2 = rmq(right(node), 1 + (visit_a + visit_b)/2, visit_b,
                want_a, want_b);
        if (partial1 == OUT_OF_BOUNDS)
            return partial2;
        if (partial2 == OUT_OF_BOUNDS)
            return partial1;

        return combine_values(partial1, partial2, 
                std::max(want_a, visit_a), std::min(want_b, visit_b));
    }

    std::vector<int> tree;
    const std::vector<int> &array;
};

int main()
{
    int n, q;
    std::vector<int> array;
    int count = -1;
    while (std::cin >> n && n)
    {
        std::cin >> q;
        array.clear();
        while(n--) {
            int temp; std::cin >> temp;
            array.push_back(temp);
        }
        segment_tree tree(array);

        while(q--) {
            count++;
            int a, b;
            std::cin >> a >> b;
            a--; b--;
            std::cout << tree.rmq(a, b) << std::endl;
        }
    }
    std::cout << std::endl;
    return 0; 
}
