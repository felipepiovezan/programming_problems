#include <iostream>
using namespace std;

int min(int a, int b)
{
    return a < b? a : b;
}

int main()
{
    int n, budget, hotels, weeks;
    while (cin >> n >> budget >> hotels >> weeks)
    {
        int sol = budget + 1;
        
        while (hotels--)
        {
            int price;
            cin >> price;
            for (int i = 0; i < weeks; i++)
            {
                int spots;
                cin >> spots;
                if (spots >= n)
                    sol = min(sol, n * price);
            }
        }
        if (sol <= budget)
            cout << sol;
        else
            cout << "stay home";
        cout << endl;
    }
}
