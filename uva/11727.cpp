#include <algorithm>
#include <iostream>
#include <vector>

int main()
{
    int n;
    int i = 1;
    std::cin >> n;

    while (n--)
    {
        std::vector<int> v(3);
        std::cin >> v[0] >> v[1] >> v[2];
        std::sort(v.begin(), v.end());
        std::cout<< "Case " << i++ << ": " << v[1] << std::endl;
    }
}
