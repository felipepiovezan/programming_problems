#include <iostream>
using namespace std;

int main()
{
    int n;
    cin >> n;

    while (n--)
    {
        int farmers;
        cin >> farmers;
        int burden = 0;

        while (farmers--)
        {
            int area, animals, env;
            cin >> area >> animals >> env;
            burden += area * env;
        }

        cout << burden << endl;
    }
}
