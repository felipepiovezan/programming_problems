#include <algorithm>
#include <cstdint>
#include <iostream>
#include <limits>
#include <vector>

template <typename IntTy> struct C23 {
  C23() {
    for (IntTy only_pow2 = 1;; only_pow2 *= 2) {
      for (IntTy both_pow2_and_3 = only_pow2;; both_pow2_and_3 *= 3) {
        powers.push_back(both_pow2_and_3);
        if (will_overflow_upon_mult(both_pow2_and_3, 3))
          break;
      }
      if (will_overflow_upon_mult(only_pow2, 2))
        break;
    }

    std::sort(std::begin(powers), std::end(powers));
  }

  void lower_bound(IntTy val) {
    std::cout << *std::lower_bound(std::begin(powers), std::end(powers), val)
              << "\n";
  }

  static bool will_overflow_upon_mult(IntTy current_val, IntTy factor) {
    return current_val > MaxInt / factor;
  }

  static constexpr auto MaxInt = std::numeric_limits<IntTy>::max();
  std::vector<IntTy> powers;
};

int main() {
  C23<uint32_t> powers;
  uint32_t query;

  while (std::cin >> query && query) {
    powers.lower_bound(query);
  }

  return 0;
}
