#include <map>
#include <iostream>
#include <stdio.h>

int main() {
    int n;
    std::cin >> n;
    std::string line;
    getline(std::cin, line);
    getline(std::cin, line);
    
    while (n--) {
        std::map<std::string, int> map;
        int count = 0;
        while(true)
        {
            getline(std::cin, line);
            if (line.size() == 0)
                break;
            map[line]++;
            count++;
        }

        for (const auto &x : map)
            printf("%s %.4f\n", x.first.c_str(), 100.0 * x.second/count);
        if (n)
            printf("\n");
    }
}
