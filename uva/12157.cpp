#include <iostream>
#include <stdio.h>
using namespace std;

int main()
{
    int t;
    cin >> t;
    int i = 0;

    while (i++ != t)
    {
        int n;
        cin >> n;
        int mile = 0, juice = 0;

        while (n--)
        {
            int duration;
            cin >> duration;
            duration++;
            mile += ((duration+29)/30) * 10;
            juice += ((duration+59)/60) * 15;
        }

        printf("Case %d: ", i);
        if (mile < juice)
            printf("Mile %d\n", mile);
        else if (juice < mile)
            printf("Juice %d\n", juice);
        else
            printf("Mile Juice %d\n", mile);
    }
}
