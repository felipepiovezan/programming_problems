#include <iostream>
#include <utility>
#include <vector>

static const std::vector<std::pair<int, int>> vertical_dir = {{0, 1}, {0, -1}};
static const std::vector<std::pair<int, int>> horizontal_dir = {{1, 0}, {-1, 0}};

struct Solver {
  int size;
  std::vector<std::string> map;

  bool visit(int row, int col,
             const std::vector<std::pair<int, int>> &directions) {
    if (row < 0 || row >= size || col < 0 || col >= size) return false;
    if (map[row][col] == '.') return false;

    bool has_ship = (map[row][col] == 'x');
    map[row][col] = '.';

    for(auto dir : directions)
      has_ship |= visit(row + dir.first, col + dir.second, directions);

    return has_ship;
  }

  bool visit(int row, int col) {
    if (map[row][col] == '.') return false;

    bool has_ship = (map[row][col] == 'x');
    map[row][col] = '.';

    for(auto dir : horizontal_dir)
      has_ship |= visit(row + dir.first, col + dir.second, horizontal_dir);
    for(auto dir : vertical_dir)
      has_ship |= visit(row + dir.first, col + dir.second, vertical_dir);

    return has_ship;
  }

  int solve() {
    int count = 0;
    for (int row = 0; row < size; row++)
      for (int col = 0; col < size; col++)
        if (visit(row, col))
          count++;
    return count;
  };
};

int main() {
  int num_tests;
  std::cin >> num_tests;

  for (int test = 1; test <= num_tests; test++) {
    int size;
    std::cin >> size;
    std::cin.ignore();
    std::vector<std::string> map;

    for (int line = 0; line < size; line++) {
      map.emplace_back("");
      std::getline(std::cin, map.back());
    }

    std::cout << "Case " << test << ": " << Solver{size, std::move(map)}.solve()
              << "\n";
  }
}
