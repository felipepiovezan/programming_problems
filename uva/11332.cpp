#include <iostream>
using namespace std;

int f(int n)
{
    int count = 0;
    while (n)
    {
        count += n%10;
        n = n/10;
    }

    return count;
}

int g(int n)
{
    if (n >= 10)
    {
        n = f(n);
        return g(n);
    }
    return n;
}

int main()
{
    int n;
    while(cin >> n && n != 0)
    {
        cout << g(n) << endl;
    }

    return 0;
}
