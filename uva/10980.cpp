#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <utility>
#include <iomanip>
using namespace std;

vector<pair<int, double>> possibilities;
map<int, double> solution;

double recurse(int query)
{
    if (query <= 0)
        return 0;
    if (solution.find(query) != solution.end())
        return solution[query];

    double ans=1e9;

    for (auto x : possibilities)
    {
        double temp = x.second + recurse(query - x.first);
        if (temp < ans)
            ans = temp;
    }
    solution[query] = ans;
    return ans;
}

int main()
{
    double price;
    int lines;
    int n;
    int test = 1;
    while (cin >> price >> lines)
    {
        cout << "Case " << test++ << ":\n";
        solution.clear();
        possibilities.resize(0);
        possibilities.push_back(make_pair(1, price));

        for (int i = 0; i < lines; ++i)
        {
            cin >> n >> price;
            possibilities.push_back(make_pair(n, price));
        }

        int query;
        string line;
        getline(cin, line);
        getline(cin, line);
        stringstream stream(line);
        std::cout << std::setprecision(2) << std::fixed;
        while (stream >> query)
        {
            cout << "Buy " << query << " for $" << recurse(query) << "\n";
        }
    }
}
