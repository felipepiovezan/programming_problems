#include <iostream>
using namespace std;

int main()
{
    int n;
    cin >> n;
    while(n--)
    {
        int x;
        cin >> x;
        int min = 1 << 30;
        int max = 0;

        while (x--)
        {
            int p;
            cin >> p;

            if (p < min)
                min = p;
            if (p > max)
                max = p;
        }
        cout << (max-min)*2 << endl;
    }
}
