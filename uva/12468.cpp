#include <iostream>
using namespace std;

int main()
{
    while (true)
    {
        int a, b;
        cin >> a >> b;
        if (a == -1)
            break;
        int diff1 = a - b;
        int diff2 = b - a;
        if (diff1 < 0)
            diff1+=100;
        if (diff2 < 0)
            diff2+= 100;

        cout << (diff1 < diff2? diff1 : diff2) << endl;
    }
}
