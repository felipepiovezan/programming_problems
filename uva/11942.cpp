#include <functional>
#include <iostream>
#include <vector>

using namespace std;

bool leq(int a, int b)
{
    return a <= b;
}

bool geq(int a, int b)
{
    return a >= b;
}

bool for_all(const std::vector<int> &v, std::function<bool(int, int)> foo)
{
    for (int i = 0; i < v.size() - 1; i++)
    {
        if (!foo(v[i], v[i+1]))
            return false;
    }
    return true;
}

int main()
{
    int n;
    cin >> n;
    std::vector<int> lumber(10);
    cout << "Lumberjacks:" << endl;
    while (n--)
    {
        for (int i = 0; i < 10; i++)
            cin >> lumber[i];
        if (for_all(lumber, leq) || for_all(lumber, geq))
            cout << "Ordered" << endl;
        else
            cout << "Unordered" << endl;
    }
}
