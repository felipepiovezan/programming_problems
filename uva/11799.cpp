#include <iostream>
#include <stdio.h>
using namespace std;

int main()
{
    int t;
    cin >> t;
    int i = 0;
    while (i++ != t)
    {
        int n;
        cin >> n;
        int max = 0;

        while (n--)
        {
            int v;
            cin >> v;
            max = max > v? max : v;
        }

        printf("Case %d: %d\n", i, max);
    }
}
