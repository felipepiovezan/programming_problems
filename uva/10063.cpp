#include <string>
#include <iostream>

using namespace std;

void recurse(const std::string &word, std::string &curr, int index = 0)
{
    if (index == word.length())
    {
        cout << curr << "\n";
        return;
    }
    char c = word[index];
    for (int i = 0; i <= curr.length(); i++)
    {
        curr.insert(curr.begin() + i, c);
        recurse(word, curr, index+1);
        curr.erase(curr.begin() + i);
    }
}

int main()
{
    string word;
    bool first = true;
    while (cin >> word)
    {
        if (first)
        {
            first = false;
        }
        else
        {
            cout << "\n";
        }
        std::string curr;
        recurse(word, curr);
    }
}
