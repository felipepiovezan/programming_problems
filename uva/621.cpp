#include <iostream>
using namespace std;

int main()
{
    int n;
    cin >> n;
    while(n--)
    {
        std::string s;
        cin >> s;
        if (s == "1" || s == "4" || s == "78")
        {
            cout << "+";
        }
        else if (s.length() > 1 && s.back() == '5' && s[s.length()-2] == '3')
        {
            cout << "-";
        }
        else if (s.front() == '9' && s.back() == '4')
        {
            cout << "*";
        }
        else
            cout << "?";
        cout<<endl;
    }
}
