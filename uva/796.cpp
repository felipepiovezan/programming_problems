#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <stdio.h>
#include <string>
#include <utility>
#include <vector>

constexpr auto UNVISITED = -1;
struct Solver {
  using vi = std::vector<int>;
  using Graph = std::vector<vi>;
  Graph G;
  vi dfs_num, dfs_low, parent;
  std::vector<std::pair<int, int>> critical;
  int next_free;

  bool read_input() {
    int num_servers;
    if (!(std::cin >> num_servers))
      return false;
    next_free = 0;
    G.clear();
    dfs_num.clear();
    dfs_low.clear();
    parent.clear();
    critical.clear();
    G.resize(num_servers);
    dfs_num.resize(num_servers, UNVISITED);
    dfs_low.resize(num_servers, UNVISITED);
    parent.resize(num_servers, UNVISITED);

    for (int i = 0; i < num_servers; i++) {
      int source;
      std::cin >> source;
      int out_deg;
      scanf(" (%d)", &out_deg);
      std::string line;
      std::getline(std::cin, line);
      auto stream = std::istringstream(line);
      G[source].insert(std::end(G[source]), std::istream_iterator<int>(stream),
                       std::istream_iterator<int>());
    }
    return true;
  }

  void print() {
    std::cout << " Num servers = " << G.size() << "\n";
    for (int i = 0; i < G.size(); i++) {
      std::cout << "Server: " << i << ": ";
      for (auto x : G[i])
        std::cout << x << ", ";
      std::cout << "\n";
    }
  }

  void solve() {
    for (int i = 0; i < G.size(); i++) {
      if (dfs_num[i] == UNVISITED)
        dfs(i);
    }
    std::sort(std::begin(critical), std::end(critical));
    std::cout << critical.size() << " critical links\n";
    for (auto link : critical)
      std::cout << link.first << " - " << link.second << "\n";
  }

  void dfs(int source) {
    dfs_num[source] = next_free++;
    dfs_low[source] = dfs_num[source];

    for (auto adj : G[source]) {
      if (dfs_num[adj] == UNVISITED) {
        parent[adj] = source;
        dfs(adj);
      }
      if (parent[source] != adj)
        if (dfs_low[adj] < dfs_low[source])
          dfs_low[source] = dfs_low[adj];

      if (dfs_low[adj] > dfs_num[source])
        critical.emplace_back(source < adj ? source : adj,
                              source > adj ? source : adj);
    }
  }
};

int main() {
  Solver solver;
  while (solver.read_input()) {
    // solver.print();
    solver.solve();
    std::cout << "\n";
  }
}
