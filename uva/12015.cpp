#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>

using namespace std;

int main()
{
    int t;
    cin >> t;
    int i = 0;
    while (i++ != t)
    {
        vector<string> sites;
        int max = 0;
        for (int i = 0; i < 10; i++)
        {
            std::string site;
            int val;
            cin >> site >> val;
            if (val == max)
                sites.push_back(site);
            else if (val > max)
            {
                sites = std::vector<string>(1, site);
                max = val;
            }
        }
        printf("Case #%d:\n", i);
        for (auto &s : sites)
            cout << s << endl;
    }
}
