#include <iostream>
using namespace std;


int main()
{
    int queries;
    while (cin >> queries && queries)
    {
        int x, y;
        cin >> x >> y;
        while (queries--)
        {
            int a, b;
            cin >> a >> b;

            if (a == x || b == y)
                cout << "divisa" << endl;
            else if (a < x)
            {
                if (b < y)
                    cout << "SO" << endl;
                else
                    cout << "NO" << endl;
            }
            else
            {
                if (b < y)
                    cout << "SE" << endl;
                else
                    cout << "NE" << endl;
            }
        }
    }
}
