#include <iostream>
#include <stdio.h>
using namespace std;

int main()
{
    int t;
    cin >> t;
    int i = 0;
    while (i++ != t)
    {
        int low = 0;
        int high = 0;
        int n;
        cin >> n;
        int first;
        cin >> first;
        n--;

        while (n--)
        {
            int next;
            cin >> next;

            if (next > first)
                high++;
            if (next < first)
                low++;
            first = next;
        }
        printf("Case %d: %d %d\n", i, high, low);
    }
}
