#include <queue>
#include <stack>
#include <iostream>
#include <vector>
using namespace std;

int n, stack_size, queue_size, total_cargos;
vector<queue<int>> cargos;

void solve()
{
    stack<int> carrier;
    int current = 0;
    int ans = 0;
    int delivered = 0;

    while (true)
    {
        if ((carrier.empty() && cargos[current].empty()) 
            || (!carrier.empty() && carrier.top() != current && cargos[current].size() == queue_size))
        {
        }
        else
        {
            while(true)
            {
                if (!carrier.empty() && carrier.top() == current)
                {
                    carrier.pop();
                    delivered++;
                    ans++;
                }
                else if(!carrier.empty() && cargos[current].size() < queue_size)
                {
                    cargos[current].push(carrier.top());
                    carrier.pop();
                    ans++;
                }
                else
                {
                    break;
                }
            }
            while(carrier.size() < stack_size && !cargos[current].empty())
            {
                carrier.push(cargos[current].front());
                cargos[current].pop();
                ans++;
            }
        }

        if (delivered == total_cargos)
            break;
        current = (current+1)%n;
        ans += 2;
    }

    cout << ans << endl;
}

int main()
{
    int tests;
    cin >> tests;

    for(int i = 0; i < tests; i++)
    {
        cin >> n >> stack_size >> queue_size;
        cargos.clear();
        cargos.resize(n);
        total_cargos = 0;
        
        for (int j = 0; j < n; j++)
        {
            int qi;
            cin >> qi;
            total_cargos += qi;
            
            while (qi--)
            {
                int x; cin >> x;
                cargos[j].push(x-1);
            }
        }

        solve();
    }
}
