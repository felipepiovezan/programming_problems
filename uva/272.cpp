#include <iostream>
#include <string>

//int main()
//{
//    char c;
//    int first = 1;
//    while (std::cin >> std::noskipws >> c)
//    {
//        if (c == '"')
//        {
//            if (first)
//                std::cout << "``";
//            else
//                std::cout << "''";
//            first = 1 - first;
//        }
//        else
//            std::cout << c;
//    }
//}


int main()
{
    char c;
    int first = 1;
    std::string s;
    while (std::cin >> s)
    {
        for (auto c : s)
        {
        if (c == '"')
        {
            if (first)
                std::cout << "``";
            else
                std::cout << "''";
            first = 1 - first;
        }
        else
            std::cout << c;
        }
        std::cout << std::endl;
    }
}
