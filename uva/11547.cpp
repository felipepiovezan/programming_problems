#include <iostream>


int main()
{
    int n;
    std::cin >> n;

    while (n--)
    {
        int x;
        std::cin >> x;
        x = (x*567/9 + 7492) * 235/47 - 498;
        if (x < 0) x *= -1;
        std::cout << (x/10)%10 <<std::endl;
    }
}
