#include <string>
#include <iostream>
#include <vector>

std::vector<int> preprocess(const std::string &pattern)
{
    const std::string &text = pattern;
    std::vector<int> fallback(pattern.size()+1, -1);

    int text_idx = 0;
    int pattern_idx = -1;

    while (text_idx < text.size())
    {
        while(pattern_idx >= 0 && text[text_idx] != pattern[pattern_idx])
            pattern_idx = fallback[pattern_idx];
        pattern_idx++;
        text_idx++;
        fallback[text_idx] = pattern_idx;
    }

    return fallback;
}

int find_second(const std::string &text, const std::string &pattern)
{
    int text_idx = 0;
    int pattern_idx = 0;
    auto fallback = preprocess(pattern);
    bool first = true;

    while (text_idx < text.size())
    {
        while(pattern_idx >= 0 && text[text_idx] != pattern[pattern_idx])
        {
            pattern_idx = fallback[pattern_idx];
        }
        pattern_idx++;
        text_idx++;

        if (pattern_idx == pattern.size())
        {
            if (first)
            {
                first = false;
                pattern_idx = fallback[pattern_idx];
            }
            else
                return text_idx - pattern_idx;
        }
    }

    return -1;
}

int main()
{
    std::string pattern, text;
    pattern.reserve(1024*1024);
    text.reserve(1024*1024*2);
    while(std::cin >> pattern && pattern != ".")
    {
        text = pattern + pattern;

        int x = find_second(text, pattern);
        printf("%d\n", pattern.size() / x); 
    }
}
