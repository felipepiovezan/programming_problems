#include <map>
#include <string>
#include <iostream>

using namespace std;

int main()
{
    map<string, string> m = {
        {"HELLO", "ENGLISH"},
        {"HOLA", "SPANISH"},
        {"HALLO", "GERMAN"},
        {"BONJOUR", "FRENCH"},
        {"CIAO", "ITALIAN"},
        {"ZDRAVSTVUJTE", "RUSSIAN"},
    };

    std::string x;
    int i = 1;
    while (cin >> x && x != "#")
    {
        cout << "Case " << i++ << ": ";
        if (m.find(x) != m.end())
        {
            cout << m[x] << endl;
        }
        else
            cout << "UNKNOWN" << endl;
    }

    return 0;
}
