#include <iostream>
#include <utility>
#include <vector>
#include <set>

constexpr std::pair<int, int> directions[] = {
    {0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}};

int flood_fill(std::vector<std::string> &map,
               std::set<std::pair<int, int>> &visited, int row, int col) {
  if (row < 0 || row >= map.size() || col < 0 || col >= map[0].size())
    return 0;
  if (visited.count(std::make_pair(row, col)) || map[row][col] == 'L')
    return 0;

  int ans = 1;
  visited.emplace(row, col);

  for (auto dir : directions)
    ans += flood_fill(map, visited, row + dir.first, col + dir.second);

  return ans;
}

void run_case() {
  std::vector<std::string> map;
  do {
    std::string line;
    std::getline(std::cin, line);
    map.push_back(std::move(line));
  } while (std::cin.peek() == 'L' || std::cin.peek() == 'W');

  do {
    int row, col;
    std::cin >> row >> col;
    row--; col--;

    std::set<std::pair<int, int>> visited;
    std::cout << flood_fill(map, visited, row, col) << "\n";

    std::cin.get();
  } while (std::isdigit(std::cin.peek()));
}

int main() {
  int num_cases;
  std::cin >> num_cases;
  std::cin.get();

  for (int i = 0; i < num_cases; i++) {
    if (i) std::cout << "\n";
    std::cin.get();
    run_case();
  }
}
