#include <iostream>
using namespace std;


int main()
{
    int duration;
    double down_pay;
    double loan;
    int records;

    while (cin >> duration >> down_pay >> loan >> records)
    {
        if (duration < 0)
            return 0;

        double value = down_pay + loan;
        double installment = loan / duration;
        int current_month = -1;
        int next_month = -1;
        double current_loss = 1;
        double next_loss;
        bool done = false;
        int ans = -1;

        while(true)
        {
            if (records == 0)
            {
                next_month = duration + 1;
                if (ans != -1)
                    break;
            }
            else
            {
                cin >> next_month >> next_loss;
                next_loss = 1 - next_loss;
                records--;
            }

            while (current_month < next_month)
            {
                value *= current_loss;
                double remaining_loan = loan - installment * (current_month);
                if (ans == -1 && remaining_loan < value)
                    ans = current_month;
                current_month++;
            }

            current_loss = next_loss;
        }

        cout << ans << " month";
        if (ans != 1) cout << "s";
        cout << endl;
    }
}
