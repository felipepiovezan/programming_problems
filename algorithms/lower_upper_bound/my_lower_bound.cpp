// Goal:
// Given a range [begin, end) and a value v, find a sub-range [v_begin, v_end)
// such that all values in that subrange are equal to v. The values in the range
// [begin, end) are assumed to be partitioned by the expression "< v".

// lower_bound(begin, end, v) = position of the first element that is not <
// than v. Or position of the first element that compares >= to v. If no
// such element exists, return end.

// upper_bound(begin, end, v) = position of the first element that is not <=
// than v. Or position of the first element that compares > v. If no such
// element exists, return end.

using Iterator = char *;
using T = char;

Iterator lower_bound(Iterator begin, Iterator end, T const &value) {
  auto distance = end - begin;

  while (distance > 0) {
    auto step = distance / 2;
    auto mid = begin + step
    if (*mid < value) {
      begin = mid;
      distance -= step + 1;
    }
    else
      distance = step;

  }

  return begin;
}
