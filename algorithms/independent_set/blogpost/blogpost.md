# Maximum Independent Set

Graphs are probably the most iconic data structure in computer science: it is an
incredibly simple-to-describe concept with a rich list of applications and open
problems. As such, it is one of the biggest themes in algorithm competitions,
like the ICPC, and in coding interviews.

I will focus on a neat implementation of a Maximum Independent
Set algorithm (as described in the [Competitive Programming 3
][0] book<sup>[0](#footnote_CP3)</sup>), how I
implemented it in C++, how I foolishly started doing code optimizations before
algorithmic optimizations, and how I paid the price for it.

In the Maximum Independent Set(MIS) problem, we are given an undirected graph
`G` and must find the **size** of the largest independent subset of vertices.
In other words, the largest subset of vertices such that no two vertices in
that set are adjacent to each other.  We will denote the size of such set by
`MIS(G)`.

[comment]: # (neato -Tpng star.dot -o star.png)
![Star graph](star.png "Star graph")

If `G` is the Star graph above, then `MIS(G) = 9` and the set itself consists
of all the blue vertices (0-8).

[comment]: # (neato -Tpng k5.dot -o k5.png)
![K5 graph](k5.png "K5 graph")

If `G` is the complete graph above, then `MIS(G) = 1` and any set with one vertex
works.

This is an NP-complete problem, so our solution will involve some sort of
complete search plus a clever prunning scheme.

## Graph data structure in C++
To start, we need an efficient way to represent edges. One way to do it is
using an adjacency matrix `AdjMatrix` of size `n x n`, where
`AdjMatrix[i][j] = 1` iff vertex `i` is adjacent to vertex `j`. However, it will be clear
later that a matrix is not good enough for our purposes.

A better approach is to transform `AdjMatrix` into a vector of bitsets. In
other words, `AdjMatrix[i]` contains `n` bits, each representing whether `i` is
connected to a particular vertex. Let's consider an example:

Suppose `n = 4` and `AdjMatrix[0] = 1011`. This means that vertex `0` is
adjacent to vertices `0, 2 and 3` (for implementation purposes, assume that
every vertex is adjacent to itself). Recall that the edges are undirected,
i.e., the 0-th bit of `AdjMatrix[2]` and `AdjMatrix[3]` must be `1` too.

With that in mind, let's look at a simple C++ graph struct that implements
this idea:

```cpp
using AdjTy = uint64_t;
struct Graph{
  const int NumVertices;
  const AdjTy AllVertices;
  std::vector<AdjTy> AdjMatrix;

  Graph(int NumVertices_) :
    NumVertices(NumVertices_),
    AllVertices(getAllVertices(NumVertices)),
    AdjMatrix(NumVertices) {}
};
```

A couple of clarifications are in order:

1. `AdjTy` is defined because we will switch to `std::bitset` when we need to
   handle bigger graphs. As the name suggests, an `uint64_t` can't handle more
   than 64 vertices.
2. The member variable `AllVertices` will help us detect a situation in
   which all vertices have been seen. It should be a sequence of exactly
   `NumVertices` bits with value `1`, and all other most significant bits should
   be `0`.

```cpp
AdjTy getAllVertices(AdjTy NumVertices) {
    return (~static_cast<AdjTy>(0)) >> (8*sizeof(AdjTy) - NumVertices);
    // If AdjTy is std::bitset<M>, where M >= NumVertices, we can use:
    return (~static_cast<AdjTy>(0)) >> (AdjTy().size()  - NumVertices);
}
```

Let's skip over how to initialize `AdjMatrix` for now, as it is a boring matter
of setting the appropriate bits. Onto the algorithm!

## The recursive algorithm

We use a recursive approach to:

* build a candidate `MIS` and
* maintain a set `Reachable` of vertices reachable from vertices in `MIS`

Notice that, by definition, vertices in `Reachable` can no longer be added to
this candidate `MIS`.

The recursion works by visiting a vertex `v` and:

0. inserting `v` into the candidate Maximum Independent Set (this is explicitly
   done since we are not interested in computing the set itself, just its
   size).
1. adding all of `v`'s neighbours to `Reachable`.
2. for each vertex `v2` whose index is greater than `v`, visit `v2` if it is
   not in `Reachable`.

Once `Reachable == AllVertices`, we stop the recursion. While that condition is
not true, there exists another vertex that can be added to the candidate set.

The bitset representation shines because it allows us to perform Step 1. very
efficiently, as we now explain. The set `Reachable` is nothing more than a
bitset of `n` bits, i.e., it's just another `AdjTy` object. If `Reachable = 11001101`
and `AdjMatrix[2] = 00111111`, then we can simply OR both of those to
compute the new value of `Reachable` when visiting vertex `2`. Instead of
adding one neighbour of `v` at a time, we add them in groups of `whatever the word size of your CPU is`.

The full code is shown below:

```cpp
using GraphTy = Graph<AdjTy>;

template<typename AdjTy>
struct RecursiveSolver {
  const GraphTy &G;
  int MIS;

  void solve() noexcept {
    MIS = 0;
    visit(0, 0, 0);
  }

  // Next = the Next vertex to consider.
  // Reachable = bitset containing the vertices that are neighbours to vertices
  //             in the current set.
  // Depth = number of vertices in the candidate set.
  void visit(int Next, AdjTy Reachable, int Depth) noexcept {
    if (Reachable == G.AllVertices) {
      MIS = std::max(MIS, Depth);
      return;
    }
    for (int i = Next; i < G.NumVertices; ++i)
      if (!get_ith_bit_of(i, Reachable))
        visit(i + 1, Reachable | G.AdjMatrix[i], Depth+1);
  }
};
```

So let's see how well we can do with this implementation. I generated random
graphs where each edge had a probability `1/Sparsity` to be present, for
values of `Sparsity` from 1 to 8. The results are summarized by the table
below<sup>[1](#footnote_bitset)</sup>. Unless stated otherwise, all times are
in seconds and each experiment was run with 50 randomly generated graphs. We
show both the average and the worst case times.

|NumVertices |Sparsity|Type  |Recursive
|------------|--------|------|---------
|64          |1       |Avg   |0.0000015
|64          |1       |Worst |0.0000022
|64          |2       |Avg   |0.0005023
|64          |2       |Worst |0.0006889
|64          |3       |Avg   |0.0074706
|64          |3       |Worst |0.0157446
|64          |4       |Avg   |0.0649518
|64          |4       |Worst |0.1124392
|64          |5       |Avg   |0.3669846
|64          |5       |Worst |0.8059119
|64          |6       |Avg   |1.4112048
|64          |6       |Worst |6.3014165
|64          |7       |Avg   |5.5429154
|64          |7       |Worst |16.1758628

The implementation gets exponentially worse as `Sparsity` increases, which
makes sense: the denser the graph, i.e. the more edges in it, the earlier the
recursion can end due to the condition `Reachable == G.AllVertices`. This is
the condition that saves us from exploring all possible `2^64` candidate sets.

## Using an explicit stack

With the basics out of the way, let's jump right into the optimizations I
foolishly tried. The first attempt was to remove the recursive calls and use an
explicit stack:

```cpp
template<typename AdjTy>
struct StackSolver {
  const GraphTy &G;
  int MIS;

  struct State {
    int Next;
    AdjTy Used;
    int Depth;
  };

  void solve() noexcept {
    MIS = 0;
 
    // 1- At any given point, the stack has at most NumVertices elements in it.
    // 2- The default underlying container (deque) is much better than a vector
    //    with pre-reserved space.
    std::stack<State> stack;
 
    stack.push({0,0,0});
    while(!stack.empty()) {
      auto Curr = stack.top(); stack.pop();
      auto Next = Curr.Next;
      auto Used = Curr.Used;
      auto Depth = Curr.Depth;

      if (Used == G.AllVertices) {
        MIS = std::max(MIS, Depth);
        continue;
      }

      for (int i = Next; i < G.NumVertices; ++i)
        if (!get_ith_bit_of(i, Used))
          stack.push({i + 1, Used | G.AdjMatrix[i], Depth + 1});
    }
  }
};
```

As the comment indicates, the recursion stack will never have more than
NumVertices elements in it - a claim I did not attempt to formally prove. I
observed it while benchmarking those two implementations, and it should
foreshadow the disapointment provoked by the next table. In hindsight, it
couldn't have been any different, otherwise we would have used the whole
program stack in the Recursive implementation.

For a fair comparison, we use the same graph for all implementations before
generating a new one.

|NumVertices |Sparsity|Type    |Recursive |Stack
|------------|--------|--------|----------|----------
|64          |1       |Avg     |0.0000015 |0.0000023
|64          |1       |Worst   |0.0000022 |0.0000157
|64          |2       |Avg     |0.0005023 |0.0005363
|64          |2       |Worst   |0.0006889 |0.0007423
|64          |3       |Avg     |0.0074706 |0.0079279
|64          |3       |Worst   |0.0157446 |0.0169251
|64          |4       |Avg     |0.0649518 |0.0693804
|64          |4       |Worst   |0.1124392 |0.1196079
|64          |5       |Avg     |0.3669846 |0.3855269
|64          |5       |Worst   |0.8059119 |0.8642856
|64          |6       |Avg     |1.4112048 |1.4410397
|64          |6       |Worst   |6.3014165 |6.2517204
|64          |7       |Avg     |5.5429154 |5.5479497
|64          |7       |Worst   |16.1758628|15.4366238

As expected, both implementations are identical.

## Pseudo-iterative implementation and threading

My next attempt was to remove the recursion altogether. Well, we can't really
remove the recursion, but we can get the compiler to do some work for us. If we
make each level of the recursion its own separate function, the compiler might
be able to generate better code. To accomplish this, we must template our
functions based on `NumVertices`.

For a lack of better terminology, I'm calling this implementation "Iterative".
It's a terrible name, doesn't really reflect what is going on, and I'm open to
suggestions.

```cpp
// In C++, one can't specialize a template member function of a template class
// without also specializing the class.
// The workaround is to use overloading instead, this empty struct helps with it.
template<int CurrVertex> struct VertexNum{};

template<int NumVertices, typename AdjTy>
struct IterativeSolver {
  const GraphTy &G;
  int MIS;

  void solve() noexcept {
    MIS = 0;
    iterate_starting_at(VertexNum<0>(), 0, 0);
  }

  template<int Next>
  void iterate_starting_at(VertexNum<Next>, AdjTy Used, int Depth) noexcept {
    if (!get_ith_bit_of(Next, Used))
      visit(VertexNum<Next+1>(), Used | G.AdjMatrix[Next], Depth+1);
    iterate_starting_at(VertexNum<Next+1>(), Used, Depth);
  }
  void iterate_starting_at(VertexNum<NumVertices>, AdjTy Used, int Depth) noexcept
  {}

  template<int Next>
  void visit(VertexNum<Next>, AdjTy Used, int Depth) noexcept {
    if (Used == G.AllVertices) {
      MIS = std::max(MIS, Depth);
      return;
    }

    if constexpr (Next < NumVertices)
      iterate_starting_at<Next>(VertexNum<Next>(), Used, Depth);
  }
  void visit(VertexNum<NumVertices+1>, AdjTy Used, int Depth) noexcept {}
};
```

The algorithm is **identical** to what we had before, so take your time to
convince yourself of this fact. All we did was rewrite the for loop as a
sequence of function calls because we know in compile time the number of
iterations that get executed. In other words, that's loop unrolling in a
`constexpr` fashion. And it does pay off, as shown in the column Iterative:

|NumVertices |Sparsity|Type    |Recursive |Stack     |Iterative|ThreadedIt
|------------|--------|--------|----------|----------|---------|----------
|64          |1       |Avg     |0.0000015 |0.0000023 |0.0000007|0.0009698
|64          |1       |Worst   |0.0000022 |0.0000157 |0.0000044|0.0012552
|64          |2       |Avg     |0.0005023 |0.0005363 |0.0002653|0.0010168
|64          |2       |Worst   |0.0006889 |0.0007423 |0.0003662|0.0011265
|64          |3       |Avg     |0.0074706 |0.0079279 |0.0034991|0.0017137
|64          |3       |Worst   |0.0157446 |0.0169251 |0.0067831|0.0028640
|64          |4       |Avg     |0.0649518 |0.0693804 |0.0269350|0.0085704
|64          |4       |Worst   |0.1124392 |0.1196079 |0.0446320|0.0172728
|64          |5       |Avg     |0.3669846 |0.3855269 |0.1416024|0.0452256
|64          |5       |Worst   |0.8059119 |0.8642856 |0.2911232|0.0923761
|64          |6       |Avg     |1.4112048 |1.4410397 |0.5376487|0.1793902
|64          |6       |Worst   |6.3014165 |6.2517204 |2.3841616|0.7270239
|64          |7       |Avg     |5.5429154 |5.5479497 |2.0659477|0.6927855
|64          |7       |Worst   |16.1758628|15.4366238|6.1887293|1.7457248

Obviously, this approach is severely flawed. Requiring `NumVertices` to be a
compile-time known value is limiting and it drastically increases compile time
if `NumVertices` is big.

Quick quiz: what will happen if we add `__attribute__((always_inline))` to
`visit` and `iterate_starting_at`? Think about it before looking at the answer
<sup>[2](#footnote_always_inline)</sup>.

At this point I should have stopped, but I didn't. I used threads on top of the
"Iterative" implementation and got some performance (and a huge increase in
compile time). The code is available on the git repository and the results are
shown in the table above as "ThreadedIt" (my machine has 4 physical cores and 4
virtual cores - i7-6700K @4.00GHz).

## Early pruning is important!

And now, for the lesson of the day: check basic pruning strategies before
optimizing the code. I came up with the following prunning strategy a bit too
late, but if you think about it for a few seconds, it's an obvious thing to do.

When we are iterating over the vertices that come after some vertex `i`, there
is no point in visiting any of them if combining `Used` with the adjacent
vertices of all future vertices will not yield `AllVertices`.  A concrete
example: suppose we have built a candidate `MIS` and we still have to visit
vertices 5, 6 and 7. If
`Used | AdjMatrix[5] | AdjMatrix[6] | AdjMatrix[7] != AllVertices`,
we can just stop the search because we will end up with a set that can be
expanded. Such a set cannot be a `MIS`.

We can perform such query in constant time with an auxiliary data structure.
First we add a member variable to our Graph class:

```cpp
  decltype(AdjMatrix) CombinedAdjacency;
```

Then we compute this CombinedAdjacency matrix using a standard "CumulativeSum"
strategy:

```cpp
void updateCombinedAdjacency() noexcept {
  CombinedAdjacency[NumVertices - 1] = AdjMatrix[NumVertices - 1];

  for (int i = NumVertices - 2; i >= 0; i--)
    CombinedAdjacency[i] = AdjMatrix[i] | CombinedAdjacency[i+1];
}
```

Our very first implementation becomes:

```cpp
void visit(int Next, AdjTy Reachable, int Depth) noexcept {
  if (Reachable == G.AllVertices) {
    MIS = std::max(MIS, Depth);
    return;
  }
  for (int i = Next; i < G.NumVertices; ++i){
    if ((Reachable | G.CombinedAdjacency[i]) != G.AllVertices) // New!
      return;
    if (!get_ith_bit_of(i, Reachable))
      visit(i + 1, Reachable | G.AdjMatrix[i], Depth+1);
  }
}
```

And this outperforms everything by orders of magnitude, to the point where I
am now bound by the time it takes me to generate the graphs. For such big
inputs, the "Iterative" versions take forever to compile due to a template
recursion that is 8k+ deep and it just performs terribly (probably due to
i-cache thrashing). The basic recursive version crashes because the stack is
not big enough. We are left with the explicit stack implementation:

|NumVertices |Sparsity|Type    |Stack
|------------|--------|--------|----------
|8192        |1       |Avg     |0.0000026
|8192        |1       |Worst   |0.0000165
|8192        |2       |Avg     |0.0005375
|8192        |2       |Worst   |0.0008693
|8192        |3       |Avg     |0.0006603
|8192        |3       |Worst   |0.0010041
|8192        |4       |Avg     |0.0006780
|8192        |4       |Worst   |0.0009479
|8192        |5       |Avg     |0.0007589
|8192        |5       |Worst   |0.0009309
|8192        |6       |Avg     |0.0007730
|8192        |6       |Worst   |0.0009526
|8192        |7       |Avg     |0.0007789
|8192        |7       |Worst   |0.0009367

## Conclusion and next steps

To be honest, I'm a bit impressed that it was possible to solve this problem
for such big inputs. At this point, we must put on the hat of a computer
theorist and try to figure out what is the worst case input for the algorithm -
it's unlikely that we will hit it by randomly generating graphs.

One graph for which where this early prunning fails is the Star Graph we
described earlier. In that graph, the center node is connected to all other
nodes and its index is the highest one, i.e. it is the last to vertex be
visited. This will cause `CombinedAdjacency[i] = AllVertices` for every `i`,
rendering our early prunning useless. It's easy to fix this, however, by
sorting vertices by their degree (number of edges coming out of it), or
randomizing the order of vertices, or stopping the recursion if
`MIS = NumVertices`
<sup>[3](#footnote_thanksgava)</sup>.

What is the worst case input for this new algorithm?  It's not feasible to
generate all possible graphs for a big enough `n`: there are `Θ(n^2!)` such
graphs if we ignore isomorphisms. The best algorithm known today, [according to
Wikipedia][1],
solves this problem in <code>O(1.1996<sup>n</sup>)</code>.

The solution was validated by solving [UVA11065][2],
a disguised MIS problem that also asks us to count the number of Maximum
Independent Sets. Counting requires a simple change to our implementation.

I hope this post was useful. All of the code is available on [GitLab](broken
link here).

---------------------
[0]: https://cpbook.net/#CP3details
[1]: https://en.wikipedia.org/wiki/Independent_set_(graph_theory)#Finding_maximum_independent_sets
[2]: https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=2006

<a name="footnote_CP3">0</a>: This is a great resource for someone who is
already familiar with most textbook algorithms, as you can quickly read it for
nice tricks, implementation details and for improved versions of well known
algorithms. It also contains a curated list of algorithm contest problems - if
you solve those, any coding interview becomes a piece of cake.

<a name="footnote_bitset">1</a>: I used a `std::bitset` instead of `uint64_t`
when running those experiments, but the results should be fairly similar in
both cases.

<a name="footnote_always_inline">2</a>: In the worst case scenario, we have
`2^64` different paths that the algorithm can take, so the compiler has to
generate a gigantic function with all of those paths. Both Clang and GCC seemed
happy to run forever, though to be fair I didn't wait long enough - there is no
way a program with `2^64` functions will fit in memory.

<a name="footnote_thanksgava">3</a>: Thanks to Gabriel Gava for mentioning this!

---------------------
