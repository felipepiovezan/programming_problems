#include <utility>
#include <vector>
#include <future>
#include "Graph.h"
#include "Solver.h"

namespace{

template<int NumVertices, typename AdjTy>
struct PartialSol {
  using GraphTy = Graph<AdjTy>;

  int MIS;
  int NIS;
  const Graph<AdjTy> &G;

  // Next = the Next vertex to consider.
  // Used = bitset containing the vertices that are neighbours to
  //             vertices in the current set.
  // Depth = number of vertices in the current set.
  template<int Next>
  void visit(VertexNum<Next>, AdjTy Used, int Depth) noexcept {
    if (Used == G.AllVertices) {
      NIS++;
      MIS = std::max(MIS, Depth);
      return;
    }

    iterate_starting_at<Next>(VertexNum<Next>(), Used, Depth);
  }
  void visit(VertexNum<NumVertices+1>, AdjTy Used, int Depth) noexcept {}

  template<int Next>
  void iterate_starting_at(VertexNum<Next>, AdjTy Used, int Depth) noexcept {
    if ((Used | G.CombinedAdjacency[Next]) != G.AllVertices)
      return;

    if (!get_ith_bit_of(Next, Used))
      visit(VertexNum<Next+1>(), Used | G.AdjMatrix[Next], Depth+1);

    iterate_starting_at(VertexNum<Next+1>(), Used, Depth);
  }
  void iterate_starting_at(VertexNum<NumVertices+1>, AdjTy Used, int Depth) noexcept
  {}

  // This is intended to be used when exploring the first vertex of a candidate
  // solution. In this case, we know that Used=0 and Depth=0. In other words,
  // we immediately add "Next" to the current IS and proceed to the next one.
  // We *must* check for the end condition before proceeding to the next one,
  // which is why visit is called.
  // This method is a way to divide the initial computation in the top most
  // branches of the recursion tree.
  // It essentially takes the Depth step without taking the width step on this
  // level of the tree.
  template<int Next>
  void start_at() noexcept {
    visit(VertexNum<Next+1>(), G.AdjMatrix[Next], 1);
  }
};
}

template<int NumVertices, typename AdjTy = uint64_t>
struct ThreadedIterativeSolver {
  using GraphTy = Graph<AdjTy>;

  const Graph<AdjTy> &G;
  int MIS;
  int NIS;
  std::vector<std::pair<int, int>> PartialSols;
  
  explicit constexpr ThreadedIterativeSolver(
      const Graph<AdjTy> &G) : G(G), MIS(0), NIS(0), PartialSols() {
    PartialSols.reserve(NumVertices);
  }

  template <int Next>
  void computeBranchStartingAt(VertexNum<Next>) {
    PartialSol<NumVertices, AdjTy> S{0,0,G};

    auto future = std::async(std::launch::async,
      [&S](){
        S.template start_at<Next>();
      }
    );

    computeBranchStartingAt(VertexNum<Next+1>());
    future.wait();
    PartialSols.emplace_back(S.MIS, S.NIS);
  }
  void computeBranchStartingAt(VertexNum<NumVertices+1>) {}

  void solve() noexcept {
    computeBranchStartingAt(VertexNum<0>());

    for (auto pair : PartialSols) {
      MIS = std::max(MIS, pair.first);
      NIS += pair.second;
    }
  }
};
