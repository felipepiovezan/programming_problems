#include "Graph.h"
#include "Solver.h"

template<int NumVertices, typename AdjTy>
struct IterativeSolver {
  using GraphTy = Graph<AdjTy>;

  const GraphTy &G;
  int MIS;
  int NIS;
  
  explicit IterativeSolver(const Graph<AdjTy> &G) : G(G),
    MIS(0), NIS(0)
  {}

  void solve() noexcept {
    MIS = 0;
    NIS = 0;
    iterate_starting_at(VertexNum<0>(), 0, 0);
  }

  // Adding __attribute__((always_inline)) on the next methods is crazy:
  // it expands all possible 2^60 calls.

  // Next = the Next vertex to consider.
  // Used = bitset containing the vertices that are neighbours to
  //             vertices in the current set.
  // Depth = number of vertices in the current set.
  template<int Next>
  void visit(VertexNum<Next>, AdjTy Used, int Depth) noexcept {
    if (Used == G.AllVertices) {
      NIS++;
      MIS = std::max(MIS, Depth);
      return;
    }

    if constexpr (Next < NumVertices)
      iterate_starting_at<Next>(VertexNum<Next>(), Used, Depth);
  }
  void visit(VertexNum<NumVertices+1>, AdjTy Used, int Depth) noexcept {}

  template<int Next>
  void iterate_starting_at(VertexNum<Next>, AdjTy Used, int Depth) noexcept {
    if ((Used | G.CombinedAdjacency[Next]) != G.AllVertices)
      return;

    if (!get_ith_bit_of(Next, Used))
      visit(VertexNum<Next+1>(), Used | G.AdjMatrix[Next], Depth+1);
    iterate_starting_at(VertexNum<Next+1>(), Used, Depth);
  }
  void iterate_starting_at(VertexNum<NumVertices>, AdjTy Used, int Depth) noexcept
  {}
};
