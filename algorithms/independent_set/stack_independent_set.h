#include <stack>

#include "Solver.h"
#include "Graph.h"

template<int NumVertices, typename AdjTy>
struct StackSolver {
  using GraphTy = Graph<AdjTy>;

  const GraphTy &G;
  int MIS;
  int NIS;
  
  StackSolver(const Graph<AdjTy> &G) : G(G), MIS(0),
    NIS(0)
  {}

  struct State {
    int Next;
    AdjTy Used;
    int Depth;
  };

  void solve() noexcept {
    MIS = 0;
    NIS = 0;
    
    // 1- At any given point, the stack has at most NumVertices elements in it.
    // 2- The default underlying container (deque) is much better than a vector
    //    with pre-reserved space.
    std::stack<State> stack;
    
    stack.push({0,0,0});
    while(!stack.empty()) {
      auto Curr = stack.top(); stack.pop(); 
      auto Next = Curr.Next;
      auto Used = Curr.Used;
      auto Depth = Curr.Depth;

      if (Used == G.AllVertices) {
        NIS++;
        MIS = std::max(MIS, Depth);
        continue;
      }

      for (int i = Next; i < NumVertices; ++i) {
        if ((Used | G.CombinedAdjacency[i]) != G.AllVertices)
          break;
        if (!get_ith_bit_of(i, Used))
          stack.push({i + 1, Used | G.AdjMatrix[i], Depth + 1});
      }
    }
  }
};
