#include <algorithm>
#include <bitset>
#include <cassert>
#include <chrono>
#include <iostream>
#include <map>
#include <stdio.h>
#include <vector>

#include "Graph.h"
#include "iterative_independent_set.h"
#include "recursive_independent_set.h"
#include "stack_independent_set_notemplate.h"
#include "threaded_iterative_independent_set.h"

struct Result{
  double time;
  int NIS;
  int MIS;
  bool valid;
};
bool operator == (const Result &r1, const Result &r2) {
  if (!r1.valid || !r2.valid) return true;
  return (r1.NIS == r2.NIS) && (r1.MIS == r2.MIS);
}

double computeAverage(int NumVertices, const std::vector<Result> &Results) {
  double AverageTime = 0.0;
  for (auto &t : Results) AverageTime += t.time;
  AverageTime/= Results.size();
  return AverageTime;
}

double computeMax(int NumVertices, const std::vector<Result> &Results) {
  double Max = 0.0;
  for (auto &t : Results) Max = std::max(Max,t.time);
  return Max;
}

template<typename Solver>
auto solve(const typename Solver::GraphTy &G) {
  auto Clock = std::chrono::steady_clock();
  Solver S{G};

  auto T1 = Clock.now();
  S.solve();
  auto T2 = Clock.now();

  Result r = {std::chrono::duration<double>(T2-T1).count(), S.NIS, S.MIS, true};
  return r;
}


enum class Implementations{
  RecursiveSolver,
  StackSolver,
  IterativeSolver,
  ThreadedIterativeSolver,
};

template <int NumVertices>
void test() {
  constexpr auto Repeat = 50;
  constexpr auto SparsityLow = 1;
  constexpr auto SparsityHigh = 8;
  using AdjTy = std::bitset<NumVertices>;

  std::map<Implementations, std::vector<Result>> Partials;

  for (auto Sparsity = SparsityLow; Sparsity < SparsityHigh; Sparsity++) {
    for (auto &pair : Partials)
      pair.second.clear();

    for (auto It = 0; It < Repeat; It++) { 
      auto G = GraphBuilder::buildRandomGraph<AdjTy>(NumVertices, Sparsity);

      auto res1 = NumVertices <= 409600 ?
        solve<RecursiveSolver<NumVertices, AdjTy>>(G) :
        Result{-1., -1, -1, false};
      Partials[Implementations::RecursiveSolver].push_back(res1);

      auto res2 = solve<StackSolver<AdjTy>>(G);
      Partials[Implementations::StackSolver].push_back(res2);
     
      auto res3 = solve<IterativeSolver<NumVertices, AdjTy>>(G);
      Partials[Implementations::IterativeSolver].push_back(res3);

      auto res4 = solve<ThreadedIterativeSolver<NumVertices, AdjTy>>(G);
      Partials[Implementations::ThreadedIterativeSolver].push_back(res4);

      assert(res1 == res2);
      //assert(res2 == res3);
      //assert(res3 == res4);
    }

    printf("%-12d%-10d%-6s", NumVertices, Sparsity, "Avg");
    for (const auto &pair : Partials)
      printf("%-12.7lf", computeAverage(NumVertices, pair.second));
    printf("\n");
    printf("%-12d%-10d%-6s", NumVertices, Sparsity, "Worst");
    for (const auto &pair : Partials)
      printf("%-12.7lf", computeMax(NumVertices, pair.second));
    printf("\n");
  }
}

int main() {
  printf("%-12s%-10s%-6s%-12s%-12s%-12s%-12s\n", "NumVertices", "Sparsity",
      "Type", "Recursive", "Stack", "Iterative", "ThreadedIt");
  test<(1 << 6)>();
  test<(1 << 7)>();
  test<(1 << 8)>();
  //test<(1 << 9)>();
  //test<(1 << 10)>();
  //test<(1 << 11)>();
  //test<(1 << 12)>();
  //test<(1 << 13)>();
  //test<(1 << 14)>();
  //test<(1 << 15)>();
}
