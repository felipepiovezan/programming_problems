#include <algorithm>
#include <bitset>
#include <iostream>
#include <stack>
#include <vector>

template <typename T>
void set_ith_bit_of(int bit, T& val) noexcept {
  val[bit] = 1;
}

template <typename T>
constexpr bool get_ith_bit_of(int bit, const T &val) noexcept {
  return val[bit];
}

template<typename AdjTy>
AdjTy getAllVertices(int NumVertices) {
  return (~static_cast<AdjTy>(0)) >> (AdjTy().size()  - NumVertices);
}

template<typename AdjTy>
struct Graph{
  const int NumVertices;
  const AdjTy AllVertices;
  std::vector<AdjTy> AdjMatrix;
  decltype(AdjMatrix) CombinedAdjacency;

  Graph(int NumVertices_) :
    NumVertices(NumVertices_),
    AllVertices(getAllVertices<AdjTy>(NumVertices)),
    AdjMatrix(NumVertices),
    CombinedAdjacency(NumVertices) {
      updateCombinedAdjacency();
    }

  void updateCombinedAdjacency() noexcept {
    CombinedAdjacency[NumVertices - 1] = AdjMatrix[NumVertices - 1];

    for (int i = NumVertices - 2; i >= 0; i--)
      CombinedAdjacency[i] = AdjMatrix[i] | CombinedAdjacency[i+1];
  }
};

template<typename AdjTy>
struct StackSolver {
  using GraphTy = Graph<AdjTy>;

  const GraphTy &G;
  const int NumVertices;
  int MIS;
  int NIS;
  
  StackSolver(const Graph<AdjTy> &G) : G(G), NumVertices(G.NumVertices), MIS(0),
    NIS(0)
  {}

  struct State {
    int Next;
    AdjTy Used;
    int Depth;
  };

  void solve() noexcept {
    MIS = 0;
    NIS = 0;
    
    // 1- At any given point, the stack has at most NumVertices elements in it.
    // 2- The default underlying container (deque) is much better than a vector
    //    with pre-reserved space.
    std::stack<State> stack;
    
    stack.push({0,0,0});
    while(!stack.empty()) {
      auto Curr = stack.top(); stack.pop(); 
      auto Next = Curr.Next;
      auto Used = Curr.Used;
      auto Depth = Curr.Depth;

      if (Used == G.AllVertices) {
        NIS++;
        MIS = std::max(MIS, Depth);
        continue;
      }

      for (int i = Next; i < NumVertices; ++i) {
        if ((Used | G.CombinedAdjacency[i]) != G.AllVertices)
          break;
        if (!get_ith_bit_of(i, Used))
          stack.push({i + 1, Used | G.AdjMatrix[i], Depth + 1});
      }
    }
  }
};

using namespace std;

template<typename AdjTy = uint64_t>
Graph<AdjTy> read() {
  int intersections; cin >> intersections;
  int roads; cin >> roads;
  Graph<AdjTy> G(intersections);

  for (int i = 0; i < intersections; i++) {
    G.AdjMatrix[i] = AdjTy(0);
    set_ith_bit_of(i, G.AdjMatrix[i]);
  }

  for (int i = 0; i < roads; i++) {
    int a, b;
    cin >> a >> b;
    set_ith_bit_of(b, G.AdjMatrix[a]);
    set_ith_bit_of(a, G.AdjMatrix[b]);
  }

  G.updateCombinedAdjacency();
  return G;
}

// Returns a NIS, MIS pair.
template <typename Solver>
std::pair<int, int> solve(Solver S) {
  S.solve();
  return {S.NIS, S.MIS};
}

template <typename AdjTy>
void solve(const Graph<AdjTy> &G) {
  auto Got = solve(StackSolver<AdjTy>(G));
  std::cout << Got.first << "\n" << Got.second << "\n";
}

int main() {
  int cities; cin >> cities;
  while (cities--) {
    auto G = read<std::bitset<64>>();
    solve(G);
 }

  return 0;
}
