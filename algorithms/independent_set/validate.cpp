#include <bitset>
#include <cassert>
#include <iostream>
#include <utility>

#include "Graph.h"

#include "iterative_independent_set.h"
#include "recursive_independent_set.h"
#include "stack_independent_set_notemplate.h"
#include "threaded_iterative_independent_set.h"

using namespace std;

template<int NumVertices, typename AdjTy = uint64_t>
auto read() {
  int intersections; cin >> intersections;
  int roads; cin >> roads;
  Graph<AdjTy> G(NumVertices);

  for (int i = 0; i < intersections; i++) {
    G.AdjMatrix[i] = AdjTy(0);
    set_ith_bit_of(i, G.AdjMatrix[i]);
  }

  for (int i = 0; i < roads; i++) {
    int a, b;
    cin >> a >> b;
    set_ith_bit_of(b, G.AdjMatrix[a]);
    set_ith_bit_of(a, G.AdjMatrix[b]);
  }

  G.updateCombinedAdjacency();
  return G;
}

// Returns a NIS, MIS pair.
template <typename Solver>
std::pair<int, int> solve(Solver S) {
  S.solve();
  return {S.NIS, S.MIS};
}

template <int NumVertices, typename AdjTy>
void solve(const Graph<AdjTy> &G, std::pair<int, int> Expected) {
  assert(Expected == solve(RecursiveSolver<NumVertices, AdjTy>(G)) && "RecursiveSolver");
  assert(Expected == solve(StackSolver<AdjTy>(G)) && "StackSolver");
  assert(Expected == solve(IterativeSolver<NumVertices, AdjTy>(G)) && "IterativeSolver");
  assert(Expected == solve(ThreadedIterativeSolver<NumVertices, AdjTy>(G)) && "ThreadedIterativeSolver");
}

int main() {
  int cities; cin >> cities;
  while (cities--) {
    switch (cities) {
      case 4: {
                auto G = read<1, std::bitset<1>>();
                solve<1>(G,{1,1});
                break;
              }
      case 3: {
                auto G = read<23, std::bitset<23>>();
                solve<23>(G,{48,3});
                break;
              }
      case 2: {
                auto G = read<13, std::bitset<13>>();
                solve<13>(G,{13,1});
                break;
              }
      case 1: {
                auto G = read<30, std::bitset<30>>();
                solve<30>(G,{50,3});
                break;
              }
      case 0: {
                auto G = read<60, std::bitset<60>>();
                solve<60>(G,{2933,9});
                break;
              }
    }
 }

  return 0;
}
