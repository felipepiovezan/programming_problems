#ifndef __GRAPH
#define __GRAPH 

#include <array>
#include <random>

// Utility struct to use in template specialization of member function
template<int CurrVertex> struct VertexNum{};

constexpr uint64_t set_nth_bit_of(int bit, uint64_t val) noexcept {
  return val | (1ull << bit);
}

constexpr bool get_nth_bit_of(int bit, uint64_t val) noexcept {
  return val & (1ull << bit);
}

template<int NumVertices>
struct Graph{
  std::array<uint64_t, NumVertices> AdjMatrix;
  static std::mt19937 gen;
  static constexpr uint64_t AllVertices = (1ull << NumVertices) - 1;

  // Resets the graph such that each Vertex is only adjacent to itself.
  // For every possible undirect edge, create it with probabilty 1/sparsity.
  void random_reset(int sparsity) noexcept {
    // resets all the MSB and ensures that i is adjacent to i;
    for (int i = 0; i < NumVertices; i++) {
      AdjMatrix[i] = set_nth_bit_of(i , 0);

      for (int j = i + 1; j < NumVertices; j++) {
        // On average, it seems that the denser the graph, the easier the problem
        // becomes.
        if ((gen() % sparsity) == 0) {
          AdjMatrix[i] = set_nth_bit_of(j, AdjMatrix[i]); 
          AdjMatrix[j] = set_nth_bit_of(i, AdjMatrix[j]);
        }
      }
    }
  } 
};

template<int NumVertices>
std::mt19937 Graph<NumVertices>::gen;

#endif
