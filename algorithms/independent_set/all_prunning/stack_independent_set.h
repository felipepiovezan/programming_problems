#include <stack>

#include "Graph.h"

template<int NumVertices>
struct StackSolver {

  const Graph<NumVertices> &G;
  int MIS;
  int NIS;
  
  constexpr StackSolver(const Graph<NumVertices> &G) : G(G), MIS(0), NIS(0) {}

  struct State {
    int next;
    uint64_t used;
    uint64_t depth;
  };

  void solve() noexcept {
    MIS = 0;
    NIS = 0;
    
    // 1- At any given point, the stack has at most NumVertices element in it.
    // 2- The default underlying container (deque) is much better than a vector
    //    with pre-reserved space.
    std::stack<State> stack;
    
    stack.push({0,0,0});
    while(!stack.empty()) {
      auto Curr = stack.top(); stack.pop(); 
      auto next = Curr.next;
      auto used = Curr.used;
      auto depth = Curr.depth;

      if (used == Graph<NumVertices>::AllVertices) {
        NIS++;
        MIS = MIS >= depth ? MIS : depth;
      }
      else {
        for (int i = next; i < NumVertices; ++i)
          if (!get_nth_bit_of(i, used))
            stack.push({i + 1, used | G.AdjMatrix[i], depth+1});
      }
    }
  }
};
