#include <utility>
#include <vector>
#include <future>
#include "Graph.h"

namespace{

template<int NumVertices>
struct PrunningPartialSol {
  int MIS;
  int NIS;
  const Graph<NumVertices> &G;
  const decltype(G.AdjMatrix) &CombinedAdjacency;

  // next = the next vertex to consider.
  // used = bitset containing the vertices that are neighbours to 
  //             vertices in the current set.
  // depth = number of vertices in the current set.
  template<int next>
    //__attribute__((always_inline))
  void recurse(VertexNum<next>, uint64_t used, int depth) noexcept {
    if (used == Graph<NumVertices>::AllVertices) {
      NIS++;
      MIS = MIS >= depth ? MIS : depth;
      return;
    }

    if ((used | CombinedAdjacency[next]) == Graph<NumVertices>::AllVertices) 
      iterate_over<next>(VertexNum<next>(), used, depth);
  }
  void recurse(VertexNum<NumVertices+1>, uint64_t used, int depth) noexcept {}

  template<int next>
    //__attribute__((always_inline))
    void iterate_over(VertexNum<next>, uint64_t used, int depth) noexcept {
      if (!get_nth_bit_of(next, used))
        recurse(VertexNum<next+1>(), used | G.AdjMatrix[next], depth+1);
      iterate_over(VertexNum<next+1>(), used, depth);
    }
  void iterate_over(VertexNum<NumVertices+1>, uint64_t used, int depth) noexcept
  {}

  // This is intended to be used when exploring the first vertex of a candidate
  // solution. In this case, we know that used=0 and depth=0. In other words,
  // we immediately add "next" to the current IS and proceed to the next one.
  // We *must* check for the end condition before proceeding to the next one,
  // which is why recurse is called.
  // This method is a way to divide the initial computation in the top most
  // branches of the recursion tree.
  // It essentially takes the depth step without taking the width step on this
  // level of the tree.
  template<int next>
  void start_at() noexcept {
    recurse(VertexNum<next+1>(), G.AdjMatrix[next], 1);
  }
};
}

template<int NumVertices>
struct PrunningThreadedIterativeSolver {
  const Graph<NumVertices> &G;
  int MIS;
  int NIS;
  decltype(G.AdjMatrix) CombinedAdjacency;
  std::vector<std::pair<int, int>> PartialSols;
  
  explicit constexpr PrunningThreadedIterativeSolver(const Graph<NumVertices> &G) : G(G), MIS(0), NIS(0),
            CombinedAdjacency(), PartialSols() {
    PartialSols.reserve(NumVertices);
  }

  template <int next>
  void computeBranchStartingAt(VertexNum<next>) {
    PrunningPartialSol<NumVertices> S{0,0,G, CombinedAdjacency};

    auto future = std::async(std::launch::async,
      [&S](){
        S.template start_at<next>();
      }
    );

    computeBranchStartingAt(VertexNum<next+1>());
    future.wait();
    PartialSols.emplace_back(S.MIS, S.NIS);
  }
  void computeBranchStartingAt(VertexNum<NumVertices+1>) {}

  void preprocess() {
    CombinedAdjacency[NumVertices - 1] = G.AdjMatrix[NumVertices - 1];

    for (int i = NumVertices - 2; i >= 0; i--)
      CombinedAdjacency[i] = G.AdjMatrix[i] | CombinedAdjacency[i+1];
  }

  void solve() noexcept {
    preprocess();
    computeBranchStartingAt(VertexNum<0>());

    for (auto pair : PartialSols) {
      MIS = std::max(MIS, pair.first);
      NIS += pair.second;
    }
  }
};
