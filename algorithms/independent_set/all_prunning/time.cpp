#include <algorithm>
#include <chrono>
#include <iostream>
#include <map>
#include <stdio.h>
#include <vector>

#include "Graph.h"

#include "iterative_independent_set.h"
#include "independent_set.h"
#include "stack_independent_set.h"
#include "threaded_iterative_independent_set.h"
#include "prunning_threaded_iterative_independent_set.h"

struct Result{
  double time;
  int NIS;
  int MIS;
};

double computeAverage(int NumVertices, const std::vector<Result> &Results) {
  double AverageTime = 0.0;
  for (auto &t : Results) AverageTime += t.time;
  AverageTime/= Results.size();
  return AverageTime;

//  auto MaxTime = std::max_element(s.begin(), Results.end(), 
//      [](const  &r1, const Result &r2) {
//        return r1.time < r2.time;
//      });
//  auto MinTime = std::min_element(s.begin(), Results.end(), 
//      [](const  &r1, const Result &r2) {
//        return r1.time < r2.time;
//      });
//  auto MaxNIS = std::max_element(s.begin(), Results.end(), 
//      [](const  &r1, const Result &r2) {
//        return r1.NIS > r2.NIS;
//      });
//  auto MaxMIS = std::max_element(s.begin(), Results.end(), 
//      [](const  &r1, const Result &r2) {
//        return r1.MIS > r2.MIS;
//      });
//
//  std::cout << "Vertices: " << NumVertices << "\n"
//            << "MaxNIS (time): " << MaxNIS->NIS << " (" << MaxNIS->time << ")\n" 
//            << "MaxMIS (time): " << MaxMIS->MIS << " (" << MaxMIS->time << ")\n"
//            << "Min,Max,Avg time: " << MinTime->time << ", " << MaxTime->time << ", " << AverageTime << std::endl; 
}

template<typename Solver, int NumVertices>
auto solve(const Graph<NumVertices> &G) {
  auto Clock = std::chrono::steady_clock();
  Solver S{G};

  auto T1 = Clock.now();
  S.solve();
  auto T2 = Clock.now();

  Result r = {std::chrono::duration<double>(T2-T1).count(), S.NIS, S.MIS};
  return r;
}

int main() {
  enum class Implementations{
    RecursiveSolver,
    IterativeSolver,
    StackSolver,
    ThreadedIterativeSolver,
    PrunningThreadedIterativeSolver,
  };
  std::map<Implementations, std::vector<double>> Times;

  constexpr auto Repeat = 5;
  constexpr auto NumVertices = 63;
  constexpr auto SparsityLow = 2;
  constexpr auto SparsityHigh = 9;

  Graph<NumVertices> G;
  std::map<Implementations, std::vector<Result>> Partials;

  for (auto Sparsity = SparsityLow; Sparsity < SparsityHigh; Sparsity++) {

    for (auto &pair : Partials)
      pair.second.clear();

    for (auto It = 0; It < Repeat; It++) { 
      G.random_reset(Sparsity);

      Partials[Implementations::RecursiveSolver].push_back(
          solve<RecursiveSolver<NumVertices>, NumVertices>(G));
      Partials[Implementations::IterativeSolver].push_back(
          solve<IterativeSolver<NumVertices>, NumVertices>(G));
      Partials[Implementations::StackSolver].push_back(
          solve<StackSolver<NumVertices>, NumVertices>(G));
      Partials[Implementations::ThreadedIterativeSolver].push_back(
          solve<ThreadedIterativeSolver<NumVertices>, NumVertices>(G));
      Partials[Implementations::PrunningThreadedIterativeSolver].push_back(
          solve<PrunningThreadedIterativeSolver<NumVertices>, NumVertices>(G));
    }

    for (const auto &pair : Partials)
      Times[pair.first].push_back(computeAverage(NumVertices, pair.second));
  }

  printf("%-10s%-20s%-20s%-20s%-20s%-20s\n", "Sparsity", "Recursive", "Iterative",
      "Stack", "ThreadedIt", "PrunningThreaded");
  for (auto Sparsity = SparsityLow; Sparsity < SparsityHigh; Sparsity++) {
    printf("%-10d", Sparsity);
    for (const auto &pair : Times)
      printf("%-20.7lf", pair.second[Sparsity-SparsityLow]);
    printf("\n");
  }
}
