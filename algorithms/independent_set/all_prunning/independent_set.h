#include "Graph.h"

template<int NumVertices>
struct RecursiveSolver {
  const Graph<NumVertices> &G;
  int MIS;
  int NIS;

  explicit constexpr RecursiveSolver(const Graph<NumVertices> &G) : G(G), MIS(0), NIS(0)
  {}

  void solve() noexcept {
    MIS = 0;
    NIS = 0;
    recurse(0, 0, 0);
  }

  // next = the next vertex to consider.
  // used = bitset containing the vertices that are neighbours to 
  //             vertices in the current set.
  // depth = number of vertices in the current set.
  void recurse(int next, uint64_t used, int depth) noexcept {
    if (used == Graph<NumVertices>::AllVertices) {
      NIS++;
      MIS = MIS >= depth ? MIS : depth;
      return;
    }

    for (int i = next; i < NumVertices; ++i)
      if (!get_nth_bit_of(i, used))
        recurse(i + 1, used | G.AdjMatrix[i], depth+1);
  }
};
