#include <iostream>
#include <utility>
#include <cassert>

#include "Graph.h"

#include "iterative_independent_set.h"
#include "independent_set.h"
#include "stack_independent_set.h"
#include "threaded_iterative_independent_set.h"
#include "prunning_threaded_iterative_independent_set.h"

using namespace std;

template<int NumVertices>
auto read() {
  int intersections; cin >> intersections;
  int roads; cin >> roads;
  Graph<NumVertices> G;

  for (int i = 0; i < intersections; i++)
    G.AdjMatrix[i] = (1ull << i);

  for (int i = 0; i < roads; i++) {
    int a, b;
    cin >> a >> b;
    G.AdjMatrix[a] = set_nth_bit_of(b, G.AdjMatrix[a]);
    G.AdjMatrix[b] = set_nth_bit_of(a, G.AdjMatrix[b]);
  }
  
  return G;
}

// Returns a NIS, MIS pair.
template <typename Solver>
std::pair<int, int> solve(Solver S) {
  S.solve();
  return {S.NIS, S.MIS};
}

template <int NumVertices>
void solve(const Graph<NumVertices> &G, std::pair<int, int> Expected) {
  assert(Expected == solve(RecursiveSolver(G)) && "RecursiveSolver");
  assert(Expected == solve(StackSolver(G)) && "StackSolver");
  assert(Expected == solve(IterativeSolver(G)) && "IterativeSolver");
  assert(Expected == solve(ThreadedIterativeSolver(G)) && "ThreadedIterativeSolver");
  assert(Expected == solve(PrunningThreadedIterativeSolver(G)) && "PrunningThreadedIterativeSolver");
}

int main() {
  int cities; cin >> cities;
  while (cities--) {
    switch (cities) {
      case 4: {
                auto G = read<1>();
                solve(G,{1,1});
                break;
              }
      case 3: {
                auto G = read<23>();
                solve(G,{48,3});
                break;
              }
      case 2: {
                auto G = read<13>();
                solve(G,{13,1});
                break;
              }
      case 1: {
                auto G = read<30>();
                solve(G,{50,3});
                break;
              }
      case 0: {
                auto G = read<60>();
                solve(G,{2933,9});
                break;
              }
    }
 }

  return 0;
}
