#include "Graph.h"

template<int NumVertices>
struct IterativeSolver {

  const Graph<NumVertices> &G;
  int MIS;
  int NIS;
  
  constexpr IterativeSolver(const Graph<NumVertices> &G) : G(G), MIS(0), NIS(0) {}

  void solve() noexcept {
    MIS = 0;
    NIS = 0;
    iterate_over(VertexNum<0>(), 0, 0);
  }

  // Adding __attribute__((always_inline)) on the next methods is crazy:
  // it expands all possible 2^60 calls.

  // next = the next vertex to consider.
  // used = bitset containing the vertices that are neighbours to 
  //             vertices in the current set.
  // depth = number of vertices in the current set.
  template<int next>
  void recurse(VertexNum<next>, uint64_t used, int depth) noexcept {
    if (used == Graph<NumVertices>::AllVertices) {
      NIS++;
      MIS = MIS >= depth ? MIS : depth;
      return;
    }

    if constexpr (next < NumVertices)
      iterate_over<next>(VertexNum<next>(), used, depth);
  }
  void recurse(VertexNum<NumVertices+1>, uint64_t used, int depth) noexcept {}

  template<int next>
  void iterate_over(VertexNum<next>, uint64_t used, int depth) noexcept {
    if (!get_nth_bit_of(next, used))
      recurse(VertexNum<next+1>(), used | G.AdjMatrix[next], depth+1);
    iterate_over(VertexNum<next+1>(), used, depth);
  }
  void iterate_over(VertexNum<NumVertices>, uint64_t used, int depth) noexcept
  {}
};
