#include "Graph.h"
#include "Solver.h"

template<int NumVertices, typename AdjTy>
struct RecursiveSolver {
  using GraphTy = Graph<AdjTy>;

  const GraphTy &G;
  int MIS;
  int NIS;

  explicit RecursiveSolver(const Graph<AdjTy> &G) noexcept : G(G),
    MIS(0), NIS(0)
  {}

  void solve() noexcept {
    MIS = 0;
    NIS = 0;
    visit(0, 0, 0);
  }

  // Next = the Next vertex to consider.
  // Reachable = bitset containing the vertices that are neighbours to
  //             vertices in the current set.
  // Depth = number of vertices in the current set.
  void visit(int Next, AdjTy Reachable, int Depth) noexcept {
    if (Reachable == G.AllVertices) {
      NIS++;
      MIS = std::max(MIS, Depth);
      return;
    }
    for (int i = Next; i < NumVertices; ++i) {
      if ((Reachable | G.CombinedAdjacency[i]) != G.AllVertices)
        return;

      if (!get_ith_bit_of(i, Reachable))
        visit(i + 1, Reachable | G.AdjMatrix[i], Depth+1);
    }
  }
};
