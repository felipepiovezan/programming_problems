#ifndef __GRAPH
#define __GRAPH 

#include <vector>
#include <random>

template <typename T>
void set_ith_bit_of(int bit, T& val) noexcept {
  if constexpr (std::is_integral<T>::value)
     val |= (static_cast<T>(1) << bit);
  else
    val[bit] = 1;
}

template <typename T>
constexpr bool get_ith_bit_of(int bit, const T &val) noexcept {
  if constexpr (std::is_integral<T>::value)
    return val & (static_cast<T>(1) << bit);
  else
    return val[bit];
}

template<typename AdjTy>
AdjTy getAllVertices(int NumVertices) {
  if constexpr (std::is_integral<AdjTy>::value)
    return (~static_cast<AdjTy>(0)) >> (8*sizeof(AdjTy) - NumVertices);
  else
    return (~static_cast<AdjTy>(0)) >> (AdjTy().size()  - NumVertices);

}

template<typename AdjTy>
struct Graph{
  const int NumVertices;
  const AdjTy AllVertices;
  std::vector<AdjTy> AdjMatrix;
  decltype(AdjMatrix) CombinedAdjacency;

  Graph(int NumVertices_) :
    NumVertices(NumVertices_),
    AllVertices(getAllVertices<AdjTy>(NumVertices)),
    AdjMatrix(NumVertices),
    CombinedAdjacency(NumVertices) {
      updateCombinedAdjacency();
    }

  void updateCombinedAdjacency() noexcept {
    CombinedAdjacency[NumVertices - 1] = AdjMatrix[NumVertices - 1];

    for (int i = NumVertices - 2; i >= 0; i--)
      CombinedAdjacency[i] = AdjMatrix[i] | CombinedAdjacency[i+1];
  }
};

namespace GraphBuilder{
  static std::mt19937 gen;

  // Resets the graph such that each Vertex is only adjacent to itself.
  // For every possible undirect edge, create it with probabilty 1/sparsity.
  // On average, it seems that the denser the graph, the easier the problem
  // becomes.
  template<typename AdjTy>
  auto buildRandomGraph(int NumVertices, int sparsity) noexcept {
    Graph<AdjTy> G{NumVertices};

    // resets all the MSB and ensures that i is adjacent to i;
    for (int i = 0; i < NumVertices; i++) {
      G.AdjMatrix[i] = AdjTy(0);
      set_ith_bit_of(i , G.AdjMatrix[i]);

      for (int j = i + 1; j < NumVertices; j++) {
        if ((gen() % sparsity) == 0) {
          set_ith_bit_of(j, G.AdjMatrix[i]);
          set_ith_bit_of(i, G.AdjMatrix[j]);
        }
      }
    }
    G.updateCombinedAdjacency();
    return G;
  }
}

#endif
