#include <vector>
#include <stdio.h>
#include <utility>

void recurse(int *first, int *last, std::vector<int> &result)
{
    if (first == last)
    {
        for(auto x : result)
            printf("%d ", x);
        printf("\n");
        return;
    }

    for(auto it = first; it < last; it++)
    {
        result.push_back(*it);
        for (auto it2 = it-1; it2 >= first; it2--)
            *(it2+1) = *it2;
        recurse(first+1, last, result);
        for (auto it2 = first; it2 < it; it2++)
            *it2 = *(it2+1);
        *it = result.back();
        result.pop_back();
    }
}

int print_all_permutations(std::vector<int> &v)
{
    auto temp = std::vector<int>();
    recurse(v.data(), v.data() + v.size(), temp);
}

int main()
{
    std::vector<int> v = {0, 1, 2, 3};
    print_all_permutations(v);
}

