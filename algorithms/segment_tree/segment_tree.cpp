#include <vector>


struct segment_tree {

    segment_tree(std::vector<int> &&v) : array(v) {
        tree.resize(4 * array.size(), 0);
        build(1, 0, array.size() - 1);
    }
    
    int rmq(int a, int b) {
        return rmq(1, 0, array.size() - 1, a, b);
    }

    private:
    int left(int node) {return 2*node;}
    int right(int node) { return 2*node +1;}

    void build(int node, int a, int b) {
        if (a == b)
            tree[node] = a;
        else {
            build(left(node), a, (a+b)/2);
            build(right(node), (a+b)/2 + 1, b);

            tree[node] = combine_values(tree[left(node)], tree[right(node)]);
        }
    }

    int combine_values(int v1, int v2)
    {
        if (array[v1] < array[v2])
            return v1;
        else
            return v2;
    }

    int rmq(int node, int visit_a, int visit_b, int want_a, int want_b) {
        if (want_b < visit_a || want_a > visit_b)
            return -1;
        if (visit_a >= want_a && visit_b <= want_b)
            return tree[node];

        int partial1 = rmq(left(node), visit_a, (visit_a + visit_b)/2, want_a,
                want_b);
        int partial2 = rmq(right(node), 1 + (visit_a + visit_b)/2, visit_b,
                want_a, want_b);
        if (partial1 == -1)
            return partial2;
        if (partial2 == -1)
            return partial1;

        return combine_values(partial1, partial2);
    }

    std::vector<int> tree;
    std::vector<int> array;
};
