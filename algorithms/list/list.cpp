#include <stdio.h>
#include <stack>

struct list_entry
{
    int val;
    list_entry *next;
};

struct list
{
    list_entry *head_;

    list() : head_(nullptr) {}

    void push_front(int val)
    {
        list_entry *entry = new list_entry();
        entry->val = val;
        entry->next = head_;
        head_ = entry;
    }

    void push_back(int val)
    {
        if (head_ == nullptr)
        {
            push_front(val);
            return;
        }

        list_entry *entry = head_;
        auto new_entry = new list_entry();
        new_entry->val = val;
        new_entry->next = nullptr;

        while(entry->next != nullptr)
        {
            entry = entry->next;
        }

        entry->next = new_entry;
    }

    void invert_list()
    {
        if (head_ == nullptr)
            return;

        std::stack<list_entry*> stack;
        auto entry = head_;

        while (entry != nullptr)
        {
            stack.push(entry);
            entry = entry->next;
        }

        head_ = stack.top();
        stack.pop();
        auto prev = head_;

        while (!stack.empty())
        {
            prev->next = stack.top();
            stack.pop();
            prev = prev->next;
        }

        prev->next = nullptr;
    }

    void print()
    {
        auto entry = head_;

        while(entry != nullptr)
        {
            printf("%d, ", entry->val);
            entry = entry->next;
        }
        printf("\n");
    }
};

int main()
{
    list l;
    l.push_back(1);
    l.push_back(2);
    l.push_back(3);
    l.push_front(0);
    l.print();
    l.invert_list();
    l.print();
}
