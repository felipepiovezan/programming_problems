#include <vector>
#include <stdio.h>

struct union_find
{
    std::vector<int> parent_;
    std::vector<int> rank_;
    union_find (int n) : parent_(n), rank_(n, 0)
    {
        for(int i = 0; i < parent_.size(); i++)
        {
            parent_[i] = i;
        }
    }

    int find_set(int x)
    {
        if (parent_[x] == x)
            return x;
        else
            return parent_[x] = find_set(parent_[x]);
    }

    bool same_set(int x, int y)
    {
        return find_set(x) == find_set(y);
    }

    void join(int x, int y)
    {
        if (same_set(x, y))
            return;

        int px = find_set(x);
        int py = find_set(y);

        if (rank_[px] < rank_[py])
        {
            std::swap(px, py);
        }

        if (rank_[px] == rank_[py])
        {
            rank_[px]++;
        }

        parent_[py] = px;
    }

    void print_sets()
    {
        for(int i = 0; i < parent_.size(); i++)
        {
            printf("parent of %d = %d\n", i , find_set(i));
        }
    }
};


int main()
{
    union_find x(10);
    x.join(1, 2);
    x.join(0, 3);
    x.join(1, 4);
    x.join(5, 4);
    x.join(6, 5);
    x.join(6, 0);
    x.print_sets();
}
