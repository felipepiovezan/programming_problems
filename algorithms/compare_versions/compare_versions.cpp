#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void compare_ver(char *str1, char *str2)
{
    char *save1, *save2;
    char *token1 = strtok_r(str1, ".", &save1);
    char *token2 = strtok_r(str2, ".", &save2);

    while (token1 || token2)
    {
        if(token1 == NULL)
        {
            printf("str1 < str2");
            return;
        }
        if(token2 == NULL)
        {
            printf("str2 < str1");
            return;
        }
        
        int val1 = atoi(token1);
        int val2 = atoi(token2);

        if(val1 < val2)
        {
            printf("str1 < str2");
            return;
        }
        if(val2 < val1)
        {
            printf("str2 < str1");
            return;
        }
        token1 = strtok_r(NULL , ".", &save1);
        token2 = strtok_r(NULL , ".", &save2);
    }

    printf("str1 = str2");
}

int main(int argv, char **argc)
{
    compare_ver(argc[1], argc[2]);
}
