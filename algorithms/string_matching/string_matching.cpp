#include <string>
#include <vector>
#include <stdio.h>

std::vector<int> preprocess(const std::string &pattern)
{
    std::vector<int> fallback(pattern.size() + 1, -1);
    const std::string &text = pattern;
    int idx_text = 0;
    int idx_pattern = -1;

    while(idx_text < text.size())
    {
        while(idx_pattern >= 0 && text[idx_text] != pattern[idx_pattern])
        {
            idx_pattern = fallback[idx_pattern];
        }

        idx_pattern++;
        idx_text++;
        fallback[idx_text] = idx_pattern;
    }
    
    return fallback;
}

void match_all(const std::string &text, const std::string &pattern)
{
    int idx_text = 0;
    int idx_pattern = 0;
    auto fallback = preprocess(pattern);

    while(idx_text < text.size())
    {
        while(idx_pattern >= 0 && text[idx_text] != pattern[idx_pattern])
        {
            idx_pattern = fallback[idx_pattern];
        }

        idx_pattern++;
        idx_text++;

        if(idx_pattern == pattern.size())
        {
            printf("Match at text idx %d\n", idx_text - idx_pattern);
            idx_pattern = fallback[idx_pattern];
        }
    }
}


int main()
{
    std::string pattern = "SEVENTYSEVEN";
    std::string text = "I DO NOT LIKE SEVENTYSEV BUT SEVENTYSEVENTYSEVEN";
    printf("text len = %d\n", text.size());
    match_all(text, pattern);
}
